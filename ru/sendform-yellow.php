<h2 class="apply_title">
	<?php
	if (isset ( $yellow_title ))
		echo $yellow_title;
	else
		echo "Продвиньте и вы свои приложения в Топ";
	?>
</h2>

<form id="send_me" class="apply_form" method="POST"
	action="//<?php echo $_SERVER['SERVER_NAME']; ?>/mail.php"
	onsubmit="yaCounter44221814.reachGoal('YELLOWFORM'); return true;">

	<div class="apply_form_controls maintenance_form_controls">
		<div class="form-group has-icon">
			<input type="text" name="name" class="form-control __no-border"
				placeholder="Ваше имя"> <span class="form-control-icon icon-user"></span>
		</div>
		<div class="form-group has-icon">
			<input type="text" name="phone" class="form-control __no-border"
				placeholder="Ваш телефон"> <span
				class="form-control-icon icon-phone"></span>
		</div>
		<div class="form-group has-icon">
			<input type="text" name="email" class="form-control __no-border"
				placeholder="Ваш e-mail" required> <span
				class="form-control-icon icon-envelope"></span>
		</div>
	</div>

	<div class="form-group form-group-button">
		<button type="submit" name="submit" class="btn btn_apply">
			<span class="icon-apply"></span>
				<?php
				if (isset ( $yellow_btn ))
					echo $yellow_btn;
				else
					echo "Отправить заявку на продвижение";
				?>
		</button>
	</div>

</form>