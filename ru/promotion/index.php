<?php
$lastmod_day = 29;
$lastmod_month = 12;
$lastmod_year = 2016;
$lastmod_min = 32;
$lastmod_hour = 16;
include ("../redirect.php");

$canonical = "//rocket4app.ru/promotion/";
$alternateEn = "//rocket4app.com/promotion/";

$page = array (
		"title" => "Заказать раскрутку мобильных игр и приложений в Google Play и AppStore",
		"description" => "Постоплата за раскрутку мобильных приложений и игр в Google Play и AppStore",
		"h1" => "<h1 class='header_info_title'>Раскрутка<br> мобильного приложения в<br> <small>Google Play<br>и AppStore <br></small></h1>", // "Продвижение Вашего мобильного приложения в топ Google Play и AppStore",
		"h2" => array (
				"0" => "<h2>Продвижение мобильного приложения наш конек!<h2>",
				"1" => "Заказать <small>раскрутку приложений Android и IOS</small>",
				"2" => "Как раскрутить приложение в Google Play &amp; AppStore?",
				"3" => "Почему важна раскрутка приложения в топ:" 
		) 
);

$yellow_title = "Раскрутите Ваше приложение с нами без предоплаты!";
$yellow_btn = "Отправить заявку на вывод приложения в Топ";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta name="referrer" content="origin">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- SEO Tags -->
<title><?php echo $page["title"]; //Главная | Rocket4App ?></title>
<meta name="description" content="<?php echo $page["description"]; ?>">
<!-- /SEO Tags -->

<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="shortcut icon" href="/favicon.ico">
		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="en"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>
		
	</head>
<body class="homepage">

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
					<?php include_once('../navigator-top.php'); ?>
					<!-- /Nav -->

				<!-- Header Info -->
				<div class="header_info">
					<div class="container">

						<div class="header_info_left">
								<?php
								// <h1 class="header_info_title">
								// Поднимем Ваше<br> мобильное приложение<br> <small>в Топ AppStore<br>
								// и Google Play
								// </small>
								// </h1>
								echo $page ["h1"];
								?>
								
								<a href="#about-top" class="btn btn_white"><span
								class="icon-more"></span>Узнайте как</a>

						</div>

						<div class="header_info_right">

							<div class="header_info_form">

								<h2 class="header_info_form_title">
										<?php
										// Оставить заявку <small>на продвижение</small>
										echo $page ["h2"] ["1"];
										?>
									</h2>

								<form method="POST" action="mail.php">

									<div class="header_info_form_controls">
										<div class="form-group has-icon">
											<input type="text" name="name" class="form-control __no-bg"
												placeholder="Ваше имя"> <span
												class="form-control-icon icon-user-white"></span>
										</div>
										<div class="form-group has-icon">
											<input type="text" name="phone" class="form-control __no-bg"
												placeholder="Ваш телефон"> <span
												class="form-control-icon icon-phone-white"></span>
										</div>
										<div class="form-group has-icon">
											<input type="text" name="email" class="form-control __no-bg"
												placeholder="Ваш e-mail"> <span
												class="form-control-icon icon-envelope-white"></span>
										</div>
									</div>

									<div class="form-group form-group-button">
										<button type="submit" name="submit" class="btn btn_apply">
											<span class="icon-apply"></span>Отправить заявку
										</button>
									</div>

								</form>

							</div>

						</div>

					</div>
				</div>
				<!-- -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Seo -->
			<div class="seo">
				<div class="container">
					<div class="in">

						<div class="seo_arrow">
							<span class="icon-arrow-down"></span>
						</div>

						<a name="about-top"></a>
						<h2 class="seo_title">
								<?php
								// Написать хорошее приложение – лишь половина успеха.<br> Вторая
								// половина – правильно его продвинуть.</strong>
								echo $page ["h2"] ["2"];
								?>
							</h2>

						<div class="divider"></div>

						<div class="seo_text">
							<p>В настоящее время на рынке существует огромное количество
								мобильных приложений и игр. Можно сколько угодно расчитывать на
								то, что Ваше приложение уникальное и сразу полюбится всему
								человечеству, однако его популярность и масштабы распространения
								зависят от площадок распространения. Публикация приложения или
								мобильной игры лишь начало долгого пути.</p>
							<br />
							<p>
								<b>Только попадание мобильного приложения в топ позволит ему
									стать популярным и прибыльным!</b>
							</p>
							<br>

							<div style="text-align: left">
								На это влияют такие критерии, как:
								<ul>
									<li>количество установок приложения</li>
									<li>положительные оценки</li>
									<li>отзывы</li>
								</ul>
							</div>

							<p>Все это мы можем обеспечить вам в кратчайшие сроки по самой
								низкой цене на рынке без предоплаты! За небольшую сумму мы дадим
								толчок Вашему приложению для попадания в топ, где оно получит
								новый импульс в виде органических установок! Остается совсем
								чуть-чуть – отдать ваш проект в руки наших специалистов и
								получить качественный, готовый результат.</p>
						</div>

					</div>
				</div>
			</div>
			<!-- /Seo -->

			<!-- How it works -->
			<div class="how-it-works">
				<div class="container">

					<h2 class="how-it-works_title section_title">
						Получите до 250% органических установок<br> <small>от вывода
							приложения в ТОП</small>
					</h2>

					<div class="how-it-works_scheme">
						<img src="//img.rocket4app.ru/images/scheme.png"
							alt="Как это работает">
					</div>

					<div class="how-it-works_text">
						<p>
							Наши технологии производят необходимое количество установок за
							кратчайшие сроки<br> для реактивного продвижения приложений в
							Топ.<br> Главная задача - получение наибольшего количества
							органических установок.
						</p>
					</div>

				</div>
			</div>
			<!-- How it works -->

			<!-- Advantages -->
			<div class="advantages inner_shadow">
				<div class="container">
						
						<?php include_once('../advantages.php'); ?>
						
					</div>
			</div>
			<!-- Advantages -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">
						
						<?php include_once('../sendform-yellow.php'); ?>
						
					</div>
			</div>
			<!-- /Apply -->

			<!-- Why -->
			<div class="why inner_shadow">
				<div class="container">

					<a name="about-us"></a>
					<h2 class="why_title">
							<?php
							// Почему стоит<br> выводить приложение в топ с нами
							echo $page ["h2"] ["3"];
							?>
						</h2>

					<ol class="why_list">
						<li>Больше половины всех установок приложений пользователи
							совершают просматривая топы</li>
						<li>Находясь в топе, приложение собирает огромный приток
							дополнительных установок</li>
						<li>Вывод в топ — самый эффективный способ продвижения приложения
							в пересчете на стоимость установки</li>
						<li>Вывод в топ — отличный повод для дополнительного пиара</li>
					</ol>

					<div class="why_rocket"></div>

				</div>
			</div>
			<!-- /Why -->

			<!-- Reviews -->
			<div class="reviews inner_shadow">
				<div class="container">
						
						<?php include_once('../reviews.php'); ?>
						
					</div>
			</div>
			<!-- /Reviews -->

			<!-- Clients -->
			<div class="clients">
				<div class="container">
						
						<?php include_once('../workwithus.php'); ?>
						
					</div>
			</div>
			<!-- /Clients -->

			<!-- Map -->
				<?php include_once('../map.php'); ?>
				<!-- /Map -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
					<?php include_once('../navigator-bottom.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="/js/owl.carousel.js"></script>
	<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>

	<!-- Callback Popup -->
		<?php include_once('../callbackwnd.php'); ?>
		<!-- /Callback Popup -->

	<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "BreadcrumbList",
				"itemListElement": [{
					"@type": "ListItem",
					"position": 1,
					"item": {
						"@id": "https://rocket4app.ru",
						"name": "Rocket4App",
							"image": "https://img.rocket4app.ru/images/maintenance_rocket.png"
					}
					},{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.ru/promotion/",
						"name": "Раскрутка мобильных приложений"
					}
				}]
			}
		</script>
</body>
</html>