<h2 class="reviews_title">Отзывы наших клиентов</h2>

<div class="reviews_carousel_wrapper">
	<div class="reviews_carousel">

		<div class="item">
			<div class="item_text">
				<cite>Работать с Rocket4App легко и приятно. От момента первого
					разговора в скайпе до достижения нужного топового результата -
					неделя времени. Все вопросы и пожелания решаются ребятами в
					оперативном режиме, отношение к клиенту - превосходное.</cite>
			</div>
			<div class="item_author">
				<div class="in">
					<div class="in_left">
						<img src="//img.rocket4app.ru/images/reviews/author-1.png"
							alt="Отзыв Андрея из Москвы">
					</div>
					<div class="in_right">
						<strong>Андрей</strong> г. Москва
					</div>
				</div>
			</div>
		</div>

		<div class="item">
			<div class="item_text">
				<cite>Мы не первый раз пользуемся услугами компаний-агергаторов
					мобильного траффика, но Rocket4App подкупают самой низкой на рынке
					продвижения мобильных приложений ценой. При этом качество и срок
					выполнения задач нисколько от дискаунта не страдают. Респект
					ребятам!</cite>
			</div>
			<div class="item_author">
				<div class="in">
					<div class="in_left">
						<img src="//img.rocket4app.ru/images/reviews/author-2.png"
							alt="Отзыв Людмилы из г.Санкт-Петербург">
					</div>
					<div class="in_right">
						<strong>Людмила</strong> г. Санкт-Петербург
					</div>
				</div>
			</div>
		</div>

		<div class="item">
			<div class="item_text">
				<cite>Лично я решился на сотрудничество с Rocket4App когда узнал,
					что оплата за продвижение моей игры производится по факту. Это
					очень круто и очень удобно - не надо трястись в ожидании "получится
					или не получится" - все риски не на стороне клиента, а на стороне
					исполнителя. Считаю, что это для российского mobile-рынка это
					большой шаг вперед.</cite>
			</div>
			<div class="item_author">
				<div class="in">
					<div class="in_left">
						<img src="//img.rocket4app.ru/images/reviews/author-3.png"
							alt="Отзыв Романа из Новосибирска">
					</div>
					<div class="in_right">
						<strong>Роман</strong> г. Новосибирск
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
