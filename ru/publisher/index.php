<?php
$lastmod_day = 18;
$lastmod_month = 4;
$lastmod_year = 2016;
$lastmod_min = 16;
$lastmod_hour = 0;
include ("../redirect.php");

$canonical = "//rocket4app.ru/publisher/";
$alternateEn = "//rocket4app.com/publisher/";

$page = array (
		"title" => "Продвинем ваше приложение в топ БЕСПЛАТНО в Google Play & AppStore по услуге Издатель",
		"description" => "Услуга 'Издатель' позволяет без вложений вывести приложение в Top GooglePlay & AppStore",
		"h1" => "<div class='header_info_title'>Бесплатное продвижение<br>мобильных игр и <br>приложений<br><small>в Google Play &amp; AppStore</small></div>", // "Продвижение Вашего мобильного приложения в топ Google Play и AppStore",
		"h2" => array (
				"0" => "<h2>Продвижение мобильного приложения наш конек!<h2>",
				"1" => "Вывести в топ <small>Андройд или iOS приложение или игру</small>",
				"2" => "Как раскрутить приложение в Google Play &amp; AppStore?",
				"3" => "Почему важна раскрутка приложения в топ:" 
		) 
);

$yellow_title = "Закажите бесплатный вывод в топ!";
$yellow_btn = "Хочу бесплатную раскрутку приложения по услуге 'Издатель 50/50'";
?>

<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta name="referrer" content="origin">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- SEO Tags -->
<title><?php echo $page["title"]; //Главная | Rocket4App ?></title>
<meta name="description" content="<?php echo $page["description"]; ?>">
<!-- /SEO Tags -->

<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="shortcut icon" href="/favicon.ico">
		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="en"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>
		<style>
p {
	padding-top: 10px;
}

blockquote {
	background-color: rgba(0, 255, 0, 0.3);
	padding: 15px;
	margin: 20px 0 0 0;
	font-size: 20px;
	font-weight: 600;
}

h3 {
	padding-top: 40px;
}

ol>li {
	font-size: 20px;
}

h3 a {
	color: #45c3c8;
}
</style>
</head>
<body class="homepage">

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
					<?php include_once('../navigator-top.php'); ?>
					<!-- /Nav -->

				<!-- Header Info -->
				<div class="header_info">
					<div class="container">

						<div class="header_info_left">
								<?php
								// <h1 class="header_info_title">
								// Поднимем Ваше<br> мобильное приложение<br> <small>в Топ AppStore<br>
								// и Google Play
								// </small>
								// </h1>
								echo $page ["h1"];
								?>
								
								<a href="#about-top" class="btn btn_white"><span
								class="icon-more"></span>Узнайте как</a>

						</div>

						<div class="header_info_right">

							<div class="header_info_form">

								<h2 class="header_info_form_title">
										<?php
										// Оставить заявку <small>на продвижение</small>
										echo $page ["h2"] ["1"];
										?>
									</h2>

								<form method="POST" action="mail.php">

									<div class="header_info_form_controls">
										<div class="form-group has-icon">
											<input type="text" name="name" class="form-control __no-bg"
												placeholder="Ваше имя"> <span
												class="form-control-icon icon-user-white"></span>
										</div>
										<div class="form-group has-icon">
											<input type="text" name="phone" class="form-control __no-bg"
												placeholder="Ваш телефон"> <span
												class="form-control-icon icon-phone-white"></span>
										</div>
										<div class="form-group has-icon">
											<input type="text" name="email" class="form-control __no-bg"
												placeholder="Ваш e-mail"> <span
												class="form-control-icon icon-envelope-white"></span>
										</div>
									</div>

									<div class="form-group form-group-button">
										<button type="submit" name="submit" class="btn btn_apply">
											<span class="icon-apply"></span>Отправить заявку
										</button>
									</div>

								</form>

							</div>

						</div>

					</div>
				</div>
				<!-- -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Seo -->
			<div class="seo">
				<div class="container">
					<div class="in">

						<div class="seo_arrow" style="margin-top: 30px;">
							<span class="icon-arrow-down"></span>
						</div>

						<a name="about-top"></a>
						<h1 class="seo_title">
								<?php
								// Написать хорошее приложение – лишь половина успеха.<br> Вторая
								// половина – правильно его продвинуть.</strong>
								echo "Хотите бесплатно вывести приложение<br> в топ Google Play или AppStore? "; // $page["h2"]["2"];
								?>
							</h1>

						<div class="divider"></div>

						<div class="seo_text" style="text-align: left">
							<p>Вы придумали чудесное мобильное приложение и теперь хотите
								забраться на верхушку Google Play или App Store, чтобы покорить
								мир и заработать денег?</p>
							<p>Увы, это не так просто. Многие замечательные программы попали
								на свалку истории, потому что у создателей не было денег на
								раскрутку своих шедевров.</p>
							<br />
							<h2 class="seo_title">Специальное предложение для разработчиков
								мобильных приложений</h2>
							<p>правильно разработанная маркетинговая стратегия и финансовая
								поддержка позволят вам не только восполнить затраты на создание
								приложения, но и заработать круглую сумму.</p>
							<p>Если у вас есть стоящая разработка, которая придется по душе
								пользователям, —</p>

						</div>

					</div>
				</div>
			</div>
			<!-- /Seo -->

			<!-- How it works -->
			<div class="how-it-works"
				style="margin-top: 130px; margin-bottom: 30px;">
				<div class="container">

					<h2 class="how-it-works_title section_title"
						style="background: url(//img.rocket4app.ru/images/section_title.png) no-repeat 50% 0;">
						Воспользуйтесь услугой «Издатель 50/50» <br> <small>от компании
							Rocket4app</small>
					</h2>

					<div class="seo_text" style="text-align: left">

						<p>Благодаря нашей поддержке, вы сможете выйти в топ магазинов
							мобильных приложений даже будучи «без гроша за душой». Главное,
							чтобы у вас была интересная идея, реализованная в программе для
							Android или iOS.</p>
						<br>
						<p>Суть услуги простая — вы разрабатываете интересный продукт, а
							мы оцениваем его перспективы и занимаемся выводом приложения в
							топ. Осуществляем публикацию приложения в google play, бесплатную
							регистрацию в App Store, задействуем аналитику. В общем —
							осуществляем раскрутку по всем направлениям.</p>
						<br>
						<p>А прибыль делим пополам. Это честное и обоюдовыгодное
							предложение, которое вы вряд ли встретите где-либо еще. Потому
							что вывод в топ нового приложения — это всегда риск.</p>
						<br>
						<p>Однако у нас достаточно опыта, чтобы чувствовать себя уверенно
							в этом рискованном бизнесе. Практические наработки, а также
							предвидение, интуиция, предчувствие позволяют нам достаточно
							точно прогнозировать успех того или иного приложения.
							Доказательства нашего профессионализма представлены на сайте в
							разделе «кейсы».</p>

					</div>
				</div>
			</div>
			<!-- /Hot-it-works -->

			<!-- Apply -->
			<div class="apply inner_shadow"
				class="margin-top:-55px;margin-bottom:30px;">
				<div class="container" id="order">
						
						<?php include_once('../sendform-yellow.php'); ?>
						
					</div>
			</div>
			<!-- /Apply -->

			<!-- Hot-it-works -->
			<div class="how-it-works"
				style="padding-top: 30px; margin-bottom: 25px;">
				<div class="container">
					<div class="seo_text" style="text-align: left">
						<h2 class="seo_title">Как заработать деньги на приложениях</h2>

						<ol>
							<li start=1>Бесплатное приложение.</li>
							<p>Вы создаете приложение с бесплатным функционалом, а
								монетизация достигается за счет дополнительных (платных) функций
								и инструментов. Аналитика мобильных приложений свидетельствует,
								что на сегодняшний день бесплатные приложения составляют 60% на
								App Store и 80% на Google Play.</p>
							<br>
							<li start=2>Платное скачивание.</li>
							<p>Этот вариант не пользуется особой популярностью, но тоже имеет
								право на существование. Чтобы эта схема работала, ваше
								приложение должно быть действительно уникальным и
								супер-интересным.</p>
							<br>
							<li start=3>Подписка.</li>
							<p>Если вы сможете постоянно наращивать контент, добавлять
								полезные услуги и взимать регулярную плату с пользователей, то —
								вперед. Но этот способ требует уйму времени, поэтому подойдет,
								разве что, издателям новостей и контент-провайдерам.</p>
							<br>
							<li start=4>Реклама внутри приложения.</li>
							<p>Вы продаете рекламное место в приложении брендам, чья
								продукция придется по душе вашей целевой аудитории.</p>
						</ol>
						<blockquote>Если у вас есть готовый продукт или он находится в
							стадии разработки, и вы хотите, чтобы он приносил деньги —
							действуйте! Возможно, как раз ваше приложение «выстрелит» и
							попадет в топ в первые же дни после публикации.</blockquote>
						<br> <br>
						<p>А мы в рамках услуги «Издатель» поможем решить все вопросы,
							касающиеся раскрутки.</p>

						<h3 class="seo_title">
							Интересно?<br> Тогда <a href="#order">заполняйте эту простую
								форму</a><br> и начинаем работать!
						</h3>
					</div>
				</div>
			</div>
			<!-- How it works -->


			<!-- Clients -->
			<div class="clients">
				<div class="container">
						
						<?php include_once('../workwithus.php'); ?>
						
					</div>
			</div>
			<!-- /Clients -->

			<!-- Map -->
				<?php include_once('../map.php'); ?>
				<!-- /Map -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
					<?php include_once('../navigator-bottom.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="/js/owl.carousel.js"></script>
	<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>

	<!-- Callback Popup -->
		<?php include_once('../callbackwnd.php'); ?>
		<!-- /Callback Popup -->

		<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "BreadcrumbList",
				"itemListElement": [{
					"@type": "ListItem",
					"position": 1,
					"item": {
						"@id": "https://rocket4app.ru",
						"name": "Rocket4App",
						"image": "https://img.rocket4app.ru/images/maintenance_rocket.png"
					}
					},{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.ru/publisher/",
						"name": "Бесплатное продвижение игр"
					}
				}]
			}
		</script>
</body>
</html>
