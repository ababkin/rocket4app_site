<?php
	
echo __FILE__;
$LastModified_unix = filemtime(__FILE__);
$LastModified = gmdate ( "D, d M Y H:i:s \G\M\T", $LastModified_unix );
echo $LastModified;

$LastModified_unix = filemtime($_SERVER['DOCUMENT_ROOT'] . "/" . basename($_SERVER['PHP_SELF']));
$LastModified = gmdate ( "D, d M Y H:i:s \G\M\T", $LastModified_unix );
$IfModifiedSince = false;
if (isset ( $_ENV ['HTTP_IF_MODIFIED_SINCE'] ))
	$IfModifiedSince = strtotime ( substr ( $_ENV ['HTTP_IF_MODIFIED_SINCE'], 5 ) );
if (isset ( $_SERVER ['HTTP_IF_MODIFIED_SINCE'] ))
	$IfModifiedSince = strtotime ( substr ( $_SERVER ['HTTP_IF_MODIFIED_SINCE'], 5 ) );
if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
	header ( $_SERVER ['SERVER_PROTOCOL'] . ' 304 Not Modified' );
	exit ();
};
echo "<!DOCTYPE HTML><!--";
echo __FILE__ . "\r\n";
echo $_SERVER['PHP_SELF'] . "\r\n";
echo $_SERVER['DOCUMENT_ROOT'] . "/" . basename(__FILE__)  . "\r\n";
echo $_SERVER['DOCUMENT_ROOT'] . "/" . basename($_SERVER['PHP_SELF']) . "\r\n";
echo "LastModified_unix:" . $LastModified_unix . "\r\n";
echo "LastModified: " . $LastModified. "\r\n";
echo "isset ( _ENV ['HTTP_IF_MODIFIED_SINCE'] ): " . (isset ( $_ENV ['HTTP_IF_MODIFIED_SINCE'] ) ? "TRUE":"FALSE") . "\r\n";
echo "IfModifiedSince: " . strtotime ( substr ( $_ENV ['HTTP_IF_MODIFIED_SINCE'], 5 ) ) . "\r\n";
echo "isset ( _SERVER ['HTTP_IF_MODIFIED_SINCE'] ): ". (isset ( $_SERVER ['HTTP_IF_MODIFIED_SINCE'] ) ? "TRUE":"FALSE")."\r\n";
echo "IfModifiedSince: ". strtotime ( substr ( $_SERVER ['HTTP_IF_MODIFIED_SINCE'], 5 ) ). "\r\n";
echo "IfModifiedSince && IfModifiedSince >= LastModified_unix: " . ($IfModifiedSince ? "TRUE":"FALSE") . " && " . ($IfModifiedSince >= $LastModified_unix ? "TRUE":"FALSE"). "\r\n";
echo "-->";


$domain = $_SERVER ['HTTP_HOST'];
$altdomain = strpos($domain, "rocket4app.ru") == 0 ? 
	str_replace("rocket4app.ru", "rocket4app.com", $domain):
	str_replace("rocket4app.com", "rocket4app.ru", $domain);
$script = $domain . $_SERVER['PHP_SELF'];
$altscript = $altdomain . $_SERVER['PHP_SELF'];

echo "Domain: ". $domain . "\r\n";
echo "Alt domain: ". $altdomain . "\r\n";
echo "Orig script: " . $script . "\r\n";
echo "Alt script: " . $altscript . "\r\n";

$domain = $_SERVER ['HTTP_HOST'];
if (strpos($domain, "rocket4app.ru") == 0)
{
	$altdomain = str_replace("rocket4app.ru", "rocket4app.com", $domain);
	$lang = "ru"; $alt_lang = "en";
}
else
{
	$altdomain = str_replace("rocket4app.com", "rocket4app.ru", $domain);
	$lang = "en"; $alt_lang = "ru";
}
$script = str_replace("/index.php", "", $domain . $_SERVER['PHP_SELF']);
$altscript = str_replace("/index.php", "", $altdomain . $_SERVER['PHP_SELF']);

echo $script . "\r\n";
echo $altscript . "\r\n";
?>