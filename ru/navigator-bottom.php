<div class="footer_left">
	
	<div class="footer_nav" id="footer_nav">
		<span>
		<ul class="footer_nav_ul">
			<li><a href="/installs/">Мотивированные установки игр и приложений</a></li>
			<li><a href="/keywords/">Продвижение поисковыми фразами</a></li>
			<li><a href="/promotion.php">О продвижении приложений</a></li>
			<li><a href="/promotion/">Раскрутка мобильных игр</a></li>
			<li><a href="/partners.php">Сотрудничество</a></li>
			<li><a href="/#about-top">О выводе в топ</a></li>
			<li><br></li>
			<!--li><a href="/prices.php">Цены</a></li-->
			<li><a href="/cases.php">Кейсы</a></li>
			<li><a href="/#about-us">Почему мы</a></li>
		</ul>
		<ul class="footer_nav_ul">
		</ul>
		</span>
	</div>
	
	<!--div class="footer_social">
		<ul class="footer_social_ul">
		<li><a href="#" class="__fb" title="Facebook"></a></li>
		<li><a href="#" class="__in" title="LinkedIn"></a></li>
		<li><a href="#" class="__tw" title="Twitter"></a></li>
		<li><a href="#" class="__yt" title="Youtube"></a></li>
		</ul>
	</div-->
	
</div>

<div class="footer_right">
	
	<div itemscope itemtype="http://schema.org/Organization">
	<span itemprop="name" style="display:none;">Rocket4App</span>
	<a href="/" class="header_logo footer_logo">
	<img src="//img.rocket4app.ru/images/logo.png" alt="Rocket4App" itemprop="logo"></a>
	<a itemprop="url" href="/"></a>
	
	<div class="header_contacts footer_contacts">
		<div class=""><b>Москва, Россия:</b> <span itemprop="telephone">+7 (495) 204-17-85</span></div>
		<div class=""><b>Киев, Украина:</b> <span itemprop="telephone">+38 (063) 183-43-62</span></div>
		<div class="header_email"><b>e-mail:</b> <a itemprop="email" href="mailto:support@rocket4app.ru" title="Написать обращение на электронную почту">support@rocket4app.ru</a></div>
		<div class="header_skype"><b>skype:</b> <a href="skype:rocket4app" title="Позвонить на скайп" >rocket4app</a></div>
		<br>	
		<div class="header_phone">Оператор в сети</div>
		<div class="header_callback footer_callback">
			<a href="#popup-callback" class="btn_callback js_callback">Перезвоните
			мне</a>
		</div>
	</div>
	</div>
</div>

<div class="" style="text-align: center;float: left;width: 70%;margin-left:15%;/*! position: relative; */margin-top: -43px;"><span style="/*! margin-right: -50%; */">Наш партнер в поисковом продвижении сайтов: <a href="http://www.semonitor.ru/promotion.html" style="text-decoration: underline;">Semonitor</a></span></div>

<script type="application/ld+json">
	{
  "@context" : "http://schema.org",
  "@type" : "Organization",
  "url" : "https://rocket4app.ru",
  "logo": "https://img.rocket4app.ru/images/logo_black.png",
  "contactPoint" : [{
    "@type" : "ContactPoint",
    "telephone" : "+74994033917",
    "contactType" : "customer support",
	"contactOption": "TollFree"
  },
  {
    "@type" : "ContactPoint",
    "telephone" : "+380631834362",
    "contactType" : "customer support",
	"contactOption": "TollFree"
  }]
}
</script>

<!-- /Google Analytics -->
<!-- script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-63938217-2', 'auto');
	ga('send', 'pageview');
	
</script -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91013877-1', 'auto');
  ga('send', 'pageview');

</script><!-- /Google Analytics -->

<!-- Код тега ремаркетинга Google -->
<!--
	С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
-->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 945538711;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
	<div style="display: inline;">
		<img height="1" width="1" style="border-style: none;" alt=""
		src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/945538711/?value=0&amp;guid=ON&amp;script=0" />
	</div>
</noscript>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
				// при смене счетчика, не забыть поменять номер счетчика в других файлах, где отслеживание событий прописано вручную
                w.yaCounter44221814 = new Ya.Metrika({
                    id:44221814,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44221814" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google Code for &#1047;&#1072;&#1103;&#1074;&#1082;&#1072; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = ‎861939651;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "TPPBCJudoHQQw8-AmwM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/861939651/?label=TPPBCJudoHQQw8-AmwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>