<?php
$lastmod_day = 9;
$lastmod_month = 6;
$lastmod_year = 2016;
$lastmod_min = 28;
$lastmod_hour = 3;
include ("redirect.php");

$canonical = "//rocket4app.ru/prices.php";
$alternateEn = "//rocket4app.com/prices.php";

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta name="referrer" content="origin">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Стоимость вывода в топ мобильных игр и приложений | Rocket4App</title>
<meta name="description"
	content="Самые низкие цены на продвижение мобильных приложений, а также бесплатная раскрутка в рамках услуги 'Издатель'">
		
		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="en"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>
		
		
		<link rel="shortcut icon" href="/favicon.ico">

<style>
a[href="#send_me"] {
	text-decoration: underline;
}
</style>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
					<?php include_once('navigator-top.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Page Header -->
			<div class="page-header">
				<div class="container">
					<div class="in">

						<h1 class="page-header_title">Стоимость услуг</h1>
						<!-- h3 class="page-header_title" style="font-size: 25px;">В цену
								входит трехдневное удержание в топе</h3>
								<h3 class="page-header_title" style="font-size: 25px;">Цены на
								вывод в других странах узнавайте у наших специалистов</h3>
								<h2 class="page-header_title" style="font-size: 25px;"><strong>Мы не берем
							предоплату</strong> - только по факту!</h2 -->
						<div class="divider"></div>

					</div>
				</div>
			</div>
			<!-- /Page Header -->

			<div class="price-advantage">
				<div class="container">
					<div class="price-adv">
						<div class="adv">
							<span class="adv1"></span>
							<h2>В цену входит трехдневное удержание в топе</h2>
						</div>
						<div class="adv">
							<span class="adv2"></span>
							<h2>Цену на ТОП в других странах узнавайте у наших специалистов</h2>
						</div>
						<div class="adv">
							<span class="adv3"></span>
							<h2>Мы знаем наше дело и всегда стараемся выжать максимум для результата!</h2>
						</div>

					</div>
				</div>
			</div>

			<!-- Pricing -->
			<div class="pricing">
				<div class="container">

					<h2 class="pricing_title __googleplay">Вывод в топ Google Play</h2>

					<table class="pricing_table">
						<thead>
							<tr>
								<th>Страна</th>
								<th>1-1000<br>установок
								</th>
								<th>1000-5000<br>установок
								</th>
								<th>более 5000<br>установок
								</th>
								<th>Top 10<br>категории
								</th>
								<th>Top 10<br> всех игр
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/russia.png"
										alt="Цена вывода в топ России для мобильных приложений Google Play"></span>
									РОССИЯ</td>
								<td class="cell_top1">5.00 руб</td>
								<td class="cell_top2">4.50 руб</td>
								<td class="cell_top1">4.00 руб</td>
								<!-- <td class="cell_top1">
										<div style="color: red;text-decoration: line-through;">1200&nbsp;$</div>
										<div style="font-size: 158%;color: #0DBF0D;">800&nbsp;$</div>
										<div style="position: relative;top: -90px;z-index: 100;left: -63px;height: 0px;">
											<img src="//img.rocket4app.ru/images/action-new.png">
										</div>
									</td>-->
								<td class="cell_top2">1000-2000&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="отправьте запрос и мы с Вами свяжемся">уточняйте</a></td>
							</tr>
							<tr class="lightblue">
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/nl.png"
										alt="Цена вывода в топ Нидерландов для мобильных приложений Google Play"></span>
									НИДЕРЛАНДЫ</td>
								<td class="cell_top1">0.18&nbsp;$</td>
								<td class="cell_top2">0.16&nbsp;$</td>
								<td class="cell_top1">0.14&nbsp;$</td>
								<td class="cell_top2">200-400&nbsp;$</td>
								<td class="cell_top3">3000&nbsp;$</td>
							</tr>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/uk.png"
										alt="Цена вывода в топ Великобритании для мобильных приложений Google Play"></span>
									ВЕЛИКОБРИТАНИЯ</td>
								<td class="cell_top1">0.18&nbsp;$</td>
								<td class="cell_top2">0.16&nbsp;$</td>
								<td class="cell_top1">0.14&nbsp;$</td>
								<td class="cell_top2">900-1800&nbsp;$</td>
								<td class="cell_top3">8 000&nbsp;$</td>
							</tr>
							<tr class="lightblue">
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/canada.png"
										alt="Цена вывода в топ Канады для мобильных приложений Google Play"></span>
									КАНАДА</td>
								<td class="cell_top1">0.18&nbsp;$</td>
								<td class="cell_top2">0.16&nbsp;$</td>
								<td class="cell_top1">0.14&nbsp;$</td>
								<td class="cell_top2">500-1000&nbsp;$</td>
								<td class="cell_top3">5 000&nbsp;$</td>
							</tr>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/germany.png"
										alt="Цена вывода в топ Германии для мобильных приложений Google Play"></span>
									ГЕРМАНИЯ</td>
								<td class="cell_top1">0.18&nbsp;$</td>
								<td class="cell_top2">0.16&nbsp;$</td>
								<td class="cell_top1">0.14&nbsp;$</td>
								<td class="cell_top2">1250-2500&nbsp;$</td>
								<td class="cell_top3">10 000&nbsp;$</td>
							</tr>
							<tr class="lightblue">
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/usa.png"
										alt="Цена вывода в топ США для мобильных приложений Google Play"></span>
									США</td>
								<td class="cell_top1">0.16&nbsp;$</td>
								<td class="cell_top2">0.14&nbsp;$</td>
								<td class="cell_top1">0.12&nbsp;$</td>
								<td class="cell_top2">5000-10000&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="отправьте запрос и мы с Вами свяжемся">уточняйте</a></td>
							</tr>
						</tbody>
					</table>


					<h2 class="pricing_title __appstore">Вывод в топ App Store</h2>

					<table class="pricing_table">
						<thead>
							<tr>
								<th>Страна</th>
								<th>За 1 простой<br> инсталл
								</th>
								<th>За 1 поисковой<br> инсталл
								</th>
								<th>TOP Трендовых<br> запросов
								</th>
							</tr>
						</thead>
						<tbody>
							<tr class="lightviolet">
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/russia.png"
										alt="Цена вывода в топ России для мобильных приложений AppStore"></span>
									РОССИЯ</td>
								<td class="cell_top1">15 руб</td>
								<td class="cell_top2">17 руб</td>
								<td class="cell_top3">45 000 руб</td>
							</tr>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/uk.png"
										alt="Цена вывода в топ Великобритании для мобильных приложений AppStore"></span>
									ВЕЛИКОБРИТАНИЯ</td>
								<td class="cell_top1">0.70&nbsp;$</td>
								<td class="cell_top2">0.80&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="отправьте запрос и мы с Вами свяжемся">уточняйте</a></td>
							</tr>
							<tr class="lightviolet">
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/canada.png"
										alt="Цена вывода в топ Канады для мобильных приложений AppStore"></span>
									КАНАДА</td>
								<td class="cell_top1">0.70&nbsp;$</td>
								<td class="cell_top2">0.80&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="отправьте запрос и мы с Вами свяжемся">уточняйте</a></td>
							</tr>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/germany.png"
										alt="Цена вывода в топ Германии для мобильных приложений AppStore"></span>
									ГЕРМАНИЯ</td>
								<td class="cell_top1">0.70&nbsp;$</td>
								<td class="cell_top2">0.80&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="отправьте запрос и мы с Вами свяжемся">уточняйте</a></td>
							</tr>
							<tr class="lightviolet">
								<td class="cell_title"><span><img
										src="//img.rocket4app.ru/images/flags/usa.png"
										alt="Цена вывода в топ США для мобильных приложений AppStore"></span>
									США</td>
								<td class="cell_top1">0.70&nbsp;$</td>
								<td class="cell_top2">0.80&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="отправьте запрос и мы с Вами свяжемся">уточняйте</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /Pricing -->

			<!-- Offers -->
			<div class="offers">
				<div class="container">

					<h2 class="offers_title">У нас есть выгодные предложения</h2>

					<div class="offers_list">

						<div class="item post __reseller">

							<div class="item_image">
								<a href="/partners.php"><img
									src="//img.rocket4app.ru/images/content/reseller.png"
									alt="Услуги раскрутки приложений для Реселлерам"></a>
							</div>

							<a href="/partners.php" class="item_title"> Для реселлеров </a>

						</div>

						<div class="item post __developer">

							<div class="item_image">
								<a href="/partners.php"><img
									src="//img.rocket4app.ru/images/content/developer.png"
									alt="Услуги раскрутки приложений для Разработчиков"></a>
							</div>

							<a href="/partners.php" class="item_title"> Для разработчиков </a>

						</div>

					</div>

				</div>
			</div>
			<!-- /Cooperation -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">
						
						<?php include_once('sendform-yellow.php'); ?>
						
					</div>
			</div>
			<!-- /Apply -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
					<?php include_once('navigator-bottom.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<!-- Callback Popup -->
		<?php include_once('callbackwnd.php'); ?>
		<!-- /Callback Popup -->

	<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<script type="application/ld+json">
		{
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [
				{
					"@type": "ListItem",
					"position": 1,
					"item": {
							"@id": "https://rocket4app.ru",
							"name": "Rocket4App",
							"image": "https://img.rocket4app.ru/images/maintenance_rocket.png"
					}
				},
				{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.ru/prices.php",
						"name": "Цены на продвижение приложений",
						"image": "https://img.rocket4app.ru/images/content/reseller.png"
					}
				}
			]
        }
	</script>
	
</body>
</html>