<?php
// Функция для проверки на существование
function exists($var) {
	// Если переменная существует
	if (isset ( $var ))
		// Возвращаем значение
		return $var;
		
		// По умолчанию
	return null;
}

// Если нажата кнопка "Finish"
if (isset ( $_POST ['submit'] )) {
	$name = exists ( $_POST ['name'] ); // First name
	$phone = exists ( $_POST ['phone'] ); // Last name
	$email = exists ( $_POST ['email'] ); // Phone number
	
	if (empty ( $name ))
		$name = "noname";
	
	if (empty ( $phone ))
		$phone = "nophone";
	
	if (empty ( $email ))
		$email = "noemail";
		
		// Заголовки (дополнительно укажете ещё)
	$headers = "Content-Type: text/plain; charset=UTF-8\r\n";
	$headers .= "From: siteform@rocket4app.ru\r\n";
	
	if (isset ( $_POST ['callback'] )) {
		// Формируем сообщение
		$message = "Заявка на обратный звонок:\r\n" . "Name: " . $name . "\r\nPhone: " . $phone . "\r\nEmail: " . $email;
	} else {
		// Формируем сообщение
		$message = "Заявка на продвижение:\r\n" . "Name: " . $name . "\r\nPhone: " . $phone . "\r\nEmail: " . $email;
	}
	
	// Отправляем
	mail ( 'support@rocket4app.ru', 'Заявка', $message, $headers );
	
	// ===== Обратное письмо ======
	
	// Заголовки (дополнительно укажете ещё)
	$headers = "Content-Type: text/plain; charset=UTF-8\r\n";
	$headers .= "From: Rocket4App Support <support@rocket4app.ru>\r\n";
	
	if (isset ( $_POST ['callback'] )) 	// заявка на обратный звонок
	{
		// Формируем сообщение
		$message = "Здравствуйте, " . $name . "!\r\n".
"Благодарим Вас за обращение! Наш специалист скоро свяжется с вами!\r\n\r\n".
"Предлагаем Вам нескольких эффективных вариантов продвижения в Google Play:\r\n".
"- Мотивированный трафик на ЛЮБУЮ страну в мире.\r\n".
"- Мощная прокачка по поисковым словам. Ваше приложение окажется в топе по запросам и увеличит вашу популярность.\r\n".
"- Вывод приложения в ТОП любой страны мира, для получения максимального количества органики.\r\n".
"- Не мотивированный трафик. Установят ваше приложение только те люди, кто действительно заинтересовался!\r\n".
"- Можем стать вашим издателем, бесплатно раскрутить интересное приложение по всему миру и разделить совместный успех!\r\n\r\n".
						
"________\r\n".
"С уважением к Вам и Вашему делу,\r\n".
"команда “Rocket4app”\r\n".
"Site: https://rocket4app.ru\r\n".
"Skype: Rocket4app\r\n".
"Email: support@rocket4app.com\r\n".
"Tel.: +7 (495) 204-17-85";
	} else {
		// Формируем сообщение
		$message = "Здравствуйте, " . $name . "!\r\n".
"Благодарим Вас за обращение! Наш специалист скоро свяжется с вами!\r\n\r\n".
"Предлагаем Вам нескольких эффективных вариантов продвижения в Google Play:\r\n".
"- Мотивированный трафик на ЛЮБУЮ страну в мире.\r\n".
"- Мощная прокачка по поисковым словам. Ваше приложение окажется в топе по запросам и увеличит вашу популярность.\r\n".
"- Вывод приложения в ТОП любой страны мира, для получения максимального количества органики.\r\n".
"- Не мотивированный трафик. Установят ваше приложение только те люди, кто действительно заинтересовался!\r\n".
"- Можем стать вашим издателем, бесплатно раскрутить интересное приложение по всему миру и разделить совместный успех!\r\n\r\n".
						
"________\r\n".
"С уважением к Вам и Вашему делу,\r\n".
"команда “Rocket4app”\r\n".
"Site: https://rocket4app.ru\r\n".
"Skype: Rocket4app\r\n".
"Email: support@rocket4app.com\r\n".
"Tel.: +7 (495) 204-17-85";
	}
	
	// Отправляем
	mail ( $email, 'Раскрутка Мобильных приложений', $message, $headers );
} else { // показываем страницу только если была отправка формы, иначе переводим на главную
	
	header ( $_SERVER ['SERVER_PROTOCOL'] . " 301 Moved Permanently" );
	header ( "Location: /" );
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Mail | Rocket4App</title>

<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="shortcut icon" href="/favicon.ico">

<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="js/main.js"></script>

</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
					<?php include_once('navigator-top.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Page Header -->
			<div class="page-header">
				<div class="container">
					<div class="in">

						<h1 class="page-header_title_med">Спасибо за обращение!</h1>
						<h1 class="page-header_title_small">Мы свяжемся с вами в ближайшее
							время!</h1>

						<div class="divider"></div>
						<div style="text-align: center;">
							<a
								style="color: rgb(0, 129, 255); font-weight: 600; text-decoration: underline;"
								href="/cases.php">Ознакомьтесь</a> с нашими успешными и
							законченными проектами
						</div>
					</div>
				</div>
			</div>
			<!-- /Page Header -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
					<?php include_once('navigator-bottom.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<!-- Callback Popup -->
		<?php include_once('callbackwnd.php'); ?>
		<!-- /Callback Popup -->

	<!-- Google Code for CPA Conversion Page -->
	<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 945538711;
			var google_conversion_language = "en";
			var google_conversion_format = "3";
			var google_conversion_color = "ffffff";
			var google_conversion_label = "NoQFCKzA014Ql43vwgM";
			var google_remarketing_only = false;
			/* ]]> */
		</script>


	<script type="text/javascript"
		src="//www.googleadservices.com/pagead/conversion.js">
		</script>
	<noscript>
		<div style="display: inline;">
			<img height="1" width="1" style="border-style: none;" alt=""
				src="//www.googleadservices.com/pagead/conversion/945538711/?label=NoQFCKzA014Ql43vwgM&amp;guid=ON&amp;script=0" />
		</div>
	</noscript>

</body>
</html>