<h2 class="advantages_title section_title">
	Наши преимущества<br> <small>для Вашего успеха!</small>
</h2>

<ul class="advantages_list">
	<li class="__1">Цена<br> вывода в Топ<br> самая низкая на рынке
	</li>
	<li class="__2">Достижение<br> отличного<br> результата
	</li>
	<li class="__3">Большой опыт<br> продвижения<br> приложений
	</li>
	<li class="__4">Объемы загрузок<br> для попадания<br> в ТОП-10
	</li>
</ul>