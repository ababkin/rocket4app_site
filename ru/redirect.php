<?php ob_start("ob_gzhandler"); ?>
<?php

$hostnodomain = "rocket4app";

/*
	if (strpos ( $_SERVER ['HTTP_HOST'], "dev" ) > 0) {
	$hostnodomain = "dev.rocket4app";
	exit ();
}
*/
unset ( $page_arr );
$path = "/";

unset($debug); //$debug = 0;

header ( "Cache-Control: max-age=864000" );
header ( "Expires: " . date ( "r", time () + 3600 * 24 * 7 ) );

$doc = $_SERVER['DOCUMENT_ROOT'] . /*basename*/($_SERVER['PHP_SELF']);

if ($debug==1) echo "<h1>$doc</h1>";

$LastModified_unix = filemtime($doc);
$LastModified = gmdate ( "D, d M Y H:i:s \G\M\T", $LastModified_unix );
$IfModifiedSince = false;
if (isset ( $_ENV ['HTTP_IF_MODIFIED_SINCE'] ))
	$IfModifiedSince = strtotime ( substr ( $_ENV ['HTTP_IF_MODIFIED_SINCE'], 5 ) );
if (isset ( $_SERVER ['HTTP_IF_MODIFIED_SINCE'] ))
	$IfModifiedSince = strtotime ( substr ( $_SERVER ['HTTP_IF_MODIFIED_SINCE'], 5 ) );
if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
	header ( $_SERVER ['SERVER_PROTOCOL'] . ' 304 Not Modified' );
	exit ();
};

header ( 'Last-Modified: ' . $LastModified );

if ($debug==1) 
{
	echo "<h1>LastModified: ". $LastModified . "</h1>";
	echo "<h1>LastModifiedUnix: " . $LastModified_unix . "</h1>";
	echo "<h1>IfModified: ". $IfModifiedSince . "</h1>";
	echo "<h1>HTTPS: " . $_SERVER["HTTPS"] . "</h1>";
	echo "<h1>HTTP: " . $_SERVER["HTTP_HOST"] . "</h1>";
	echo "<h1>QUERY: ". $_SERVER["QUERY_STRING"]. "</h1>";
	echo "<h1>URI: " . $_SERVER ['HTTP_HOST'] . $_SERVER["PHP_SELF"] . "?" . $_SERVER["QUERY_STRING"] . "</h1>";
	echo "<h1>base: " . $_SERVER ['HTTP_HOST'] . basename($_SERVER["PHP_SELF"]) . "?" . $_SERVER["QUERY_STRING"] . "</h1>";
}

$hard_location = $orig_location = ($_SERVER["HTTPS"] == "" ? "http://": "https://") . $_SERVER ['HTTP_HOST'] . $_SERVER["PHP_SELF"] . ($_SERVER["QUERY_STRING"] != "" ? "?" . $_SERVER["QUERY_STRING"] : "");
if ($debug==1) echo  "<h1>REQUEST: $orig_location </h1>";
// 1. все http редиректы делаем на https
if ($_SERVER["HTTPS"] == "")
{
	$hard_location = "https://" . $_SERVER ['HTTP_HOST'] . $_SERVER["PHP_SELF"] . ($_SERVER["QUERY_STRING"] != "" ? "?" . $_SERVER["QUERY_STRING"] : "");
	if ($debug==1) echo "<h1>hard1: $hard_location</h1>";
}

// 2. все www. делаем без www
if ($debug==1) echo strpos($_SERVER["HTTP_HOST"], "www.") !== false ? "www!" : "not www";
if (strpos($_SERVER["HTTP_HOST"], "www.")  !== false)
{
	$hard_location = str_replace("www.", "", $hard_location);
	if ($debug==1) echo "<h1>hard2: $hard_location</h1>";
}

if (strpos($_SERVER["PHP_SELF"], basename($_SERVER["PHP_SELF"]) . "/"))
{
	$hard_location = str_replace(basename($_SERVER["PHP_SELF"]) . "/", basename($_SERVER["PHP_SELF"]), $hard_location);
	if ($debug==1) echo "<h1>hard3: $hard_location</h1>";	
}

if ($hard_location != $orig_location)
{
	header ( $_SERVER ['SERVER_PROTOCOL'] . " 301 Moved Permanently" );
	header ( "Location: " . $hard_location);
	if ($debug==1) echo "<h1>HARD REDIRECT!</h1>";
	exit();
}

$page = $_SERVER ["PHP_SELF"];
if ($debug==1) 
{
	echo "<h1>" . $page ."</h1>";
	echo "[proto=".$_SERVER["SERVER_PROTOCOL"]."]";
	echo "[server=".$_SERVER["SERVER_NAME"]."]";
	echo "[uri=".$_SERVER["PHP_SELF"]."]";
	echo "[query=/?".$_SERVER["QUERY_STRING"]."]";
	echo "[host=".$_SERVER["HTTP_HOST"]."]";
	echo "[https=".$_SERVER["HTTPS"]."]";
	echo "[" . strpos($_SERVER["HTTP_HOST"], "www.") . "]";
}

if ($page == "/redirect.php")
{
	header ( $_SERVER ['SERVER_PROTOCOL'] . " 301 Moved Permanently" );
	header ( "Location: /" );
	exit ();	
}
if (/*($page == "/googleplay/")||*/($page == "/appstore/")/*||($page == "/installs/")*/)
	{
	// echo "301";
	header ( $_SERVER ['SERVER_PROTOCOL'] . " 302 Moved Permanently" );
	header ( "Location: /" );
	exit ();
}
if ($page == "/games/") {
	header ( $_SERVER ['SERVER_PROTOCOL'] . " 301 Moved Permanently" );
	header ( "Location: /googleplay/" );
	exit ();
}
if (($page == "/cases/")) {
	header ( $_SERVER ['SERVER_PROTOCOL'] . " 301 Moved Permanently" );
	header ( "Location: /cases.php" );
	exit ();
}

if (! file_exists ( $doc ) && ! file_exists($doc . "index.php")) {
	header ( $_SERVER ['SERVER_PROTOCOL'] . " 404 Not found" );
	// echo "<h1>/var/www/html".$path."</h1>";
	// echo "<h1>/var/www/html".$path."index.php</h1>";
	// echo "404";
	exit ();
}

//exit();

?>	