<?php
	
	$lastmod_day = 17; $lastmod_month = 2; $lastmod_year = 2016;
	$lastmod_min = 39; $lastmod_hour = 2;
	include ("redirect.php");
	
	$canonical = "//rocket4app.ru/promotion.php";
	$alternateEn = "//rocket4app.com/promotion.php";
	
	$page = array(
	"title" => "Заказать продвижение мобильных приложений в топ Google Play & AppStore с Rocket4App!",
	"description" => "Постоплата мобильного продвижения! Самые низкие цены! Услуга издатель! Небольшая команда профессионалов с огромным опытом поднимет ваше приложение в топ любого магаина - Rocket4App",
	"h1" => "<h1 class='header_info_title' style='width: 390px;'>Не знаете как попасть в Топ Google Play и AppStore? <br/><small>Мы вам поможем!</small></h1>",//"Продвижение Вашего мобильного приложения в топ Google Play и AppStore",
	"h2" => array(
	"0" => "<h2>Продвижение мобильного приложения наш конек!<h2>",
	"1" => "Оставить заявку <br>на продвижение <br><small style='padding-top:10px'>мобильного приложения или игры</small>",
	"2" => "Написать <strong>хорошее приложение</strong> – лишь половина успеха.<br> Вторая	половина – <strong>успешное продвижение приложения!</strong>",
	"3" => "Почему стоит<br> продвигать приложение с нами:"
	)
	);
	
	$yellow_title = "Продвинуть приложение в Топ прямо сейчас!";
?>

<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width">
		<meta name="referrer" content="origin">
		<?php //<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<!-- SEO Tags -->
		<title><?php echo $page["title"]; //Главная | Rocket4App ?></title>
		<meta name="description" content="<?php echo $page["description"]; ?>" >
		<!-- /SEO Tags -->
		<!-- OG Tags -->
		<meta http-equiv="content-language" content="ru">
		<meta property="og:site_name" content="Rocket4App"/>
		<meta property="og:title" content="Rocket4App - продвижение мобильных приложений и игр"/>
		<meta property="og:image" content="//img.rocket4app.ru/images/logo.jpg"/>
		<meta property="og:description" content="Бесплатный вывод приложений в ТОП."/>
		<meta property="og:url" content="//<?php echo $_SERVER ['SERVER_NAME']; ?>/"/>
		<!-- /OG Tags -->
		
		<link rel="shortcut icon" href="/favicon.ico">
		<?php if (isset($canonical)): ?><link rel="canonical" href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="en" href="<?php echo $alternateEn; ?>"/><?php endif; ?>
		
		<link rel="stylesheet" type="text/css" href="/css/style.css">
		
		<script type="text/javascript" src="js/jquery-1.9.0.min.js" async></script>
		<script type="text/javascript" src="js/jquery.placeholder.min.js" async></script>
		<script type="text/javascript" src="js/owl.carousel.js" async></script>
		<script type="text/javascript" src="js/jquery.fancybox.pack.js" async></script>
		<script type="text/javascript" src="js/main.js" async></script>
		
		<style>
		.blue_links a { color:#0184D7;font-weight:600; };
		</style>
	</head>
	<body class="homepage">
		
		<!-- Wrapper -->
		<div class="wrapper">
			
			<!-- Header -->
			<section>
			<div class="header">
				<div class="container">
					
					<!-- Nav -->
					<?php include_once('navigator-top.php'); ?>
					<!-- /Nav -->
					
					<!-- Header Info -->
					<div class="header_info">
						<div class="container">
							
							<div class="header_info_left">
								<?php
									//<h1 class="header_info_title">
									//	Поднимем Ваше<br> мобильное приложение<br> <small>в Топ AppStore<br>
									//		и Google Play 
									//	</small>
									//</h1>
									echo $page["h1"];
								?>
								
								<a href="#about-top" class="btn btn_white"><span
								class="icon-more"></span>Узнайте как</a>
								
							</div>
							
							<div class="header_info_right">
								
								<div class="header_info_form">
									
									<h2 class="header_info_form_title">
										<?php
											//Оставить заявку <small>на продвижение</small>
											echo $page["h2"]["1"];
										?>
									</h2>
									
									<form method="POST" action="mail.php">
										
										<div class="header_info_form_controls">
											<div class="form-group has-icon">
												<input type="text" name="name" class="form-control __no-bg"
												placeholder="Ваше имя"> <span
												class="form-control-icon icon-user-white"></span>
											</div>
											<div class="form-group has-icon">
												<input type="text" name="phone" class="form-control __no-bg"
												placeholder="Ваш телефон"> <span
												class="form-control-icon icon-phone-white"></span>
											</div>
											<div class="form-group has-icon">
												<input type="text" name="email" class="form-control __no-bg"
												placeholder="Ваш e-mail"> <span
												class="form-control-icon icon-envelope-white"></span>
											</div>
										</div>
										
										<div class="form-group form-group-button">
											<button type="submit" name="submit" class="btn btn_apply">
												<span class="icon-apply"></span>Отправить заявку
											</button>
										</div>
										
									</form>
									
								</div>
								
							</div>
							
						</div>
					</div>
					<!-- -->
					
				</div>
			</div>
			</section>
			<!-- /Header -->
			
			<!-- Content -->
			<div class="content">
				
				<!-- Seo -->
				<section>
				<div class="seo">
					<div class="container">
						<div class="in">
							
							<div class="seo_arrow">
								<span class="icon-arrow-down"></span>
							</div>
							
							<a id="about-top"></a>
							<h2 class="seo_title">
								<?php
									//Написать хорошее приложение – лишь половина успеха.<br> Вторая
									//половина – правильно его продвинуть.</strong>
									echo $page["h2"]["2"];
								?>
							</h2>
							
							<div class="divider"></div>
							
							<div class="seo_text blue_links">
								<p>Правильный подход к <a href="/promotion/" title="Раскрутить мобильное приложение с Rocket4App">
									раскрутке приложений</a> – залог успеха вашего
									будущего проекта. Даже, если проект идеален, сразу никто о нем
									не узнает. А мы запросто сделаем так, чтобы люди не только
									узнали, но и скачивали ваше приложение. У нас одна из самых
									низких стоимостей инсталла на рынке – это политика, которой
									придерживается компания для продвижения собственных продуктов.
									Мы делаем это по <a href="/prices.php" title="Ознакомьтесь с нашими ценами на раскрутку приложений и игр">разумной цене</a>, так как уверены в качестве
									собственных реализованных <a href="/cases.php" title="Более подробно о наших кейсах">проектов</a>. Остается совсем чуть-чуть –
									отдать ваш проект в руки наших специалистов и получить
								качественный, готовый результат!</p>
							</div>
							
						</div>
					</div>
				</div>
				</section>
				<!-- /Seo -->
				
				<!-- How it works -->
				<section>
				<div class="how-it-works">
					<div class="container">
						
						<h2 class="how-it-works_title section_title">
							Получите до 250% органических установок<br> <small>от вывода
							приложения в ТОП</small>
						</h2>
						
						<div class="how-it-works_scheme">
							<img src="//img.rocket4app.ru/images/scheme.png" alt="Как это работает">
						</div>
						
						<div class="how-it-works_text">
							<h1 style="font-weight: 600;font-size: 110%;">Продвижение мобильных приложений Андроид в Google Play и IOS в AppStore с компанией Rocket4App</h1>
							<p>
								Наши технологии производят необходимое количество установок за
								кратчайшие сроки<br> для реактивного продвижения приложений в
								Топ.<br> Главная задача - получение наибольшего количества
								органических установок.
							</p>
						</div>
						
					</div>
				</div>
				</section>
				<!-- How it works -->
				
				<!-- Advantages -->
				<div class="advantages inner_shadow">
					<div class="container">
						
						<?php include_once('advantages.php'); ?>
						
					</div>
				</div>
				<!-- Advantages -->
				
				<!-- Apply -->
				<div class="apply inner_shadow">
					<div class="container">
						
						<?php include_once('sendform-yellow.php'); ?>
						
					</div>
				</div>
				<!-- /Apply -->
				
				<!-- Why -->
				<section>
				<div class="why inner_shadow">
					<div class="container">
						
						<a id="about-us"></a>
						<h2 class="why_title">
							<?php
								//Почему стоит<br> выводить приложение в топ с нами
								echo $page["h2"]["3"];
							?>
						</h2>
						
						<ol class="why_list">
							<li>Больше половины всех установок приложений пользователи
							совершают просматривая топы</li>
							<li>Находясь в топе, приложение собирает огромный приток
							дополнительных установок</li>
							<li>Вывод в топ — самый эффективный способ продвижения приложения
							в пересчете на стоимость установки</li>
							<li>Нахождение в топ — отличный повод для дополнительного пиара</li>
						</ol>
						
						<div class="why_rocket"></div>
						
					</div>
				</div>
				</section>
				<!-- /Why -->
				
				<!-- Reviews -->
				<div class="reviews inner_shadow">
					<div class="container">
						
						<?php include_once('reviews.php'); ?>
						
					</div>
				</div>
				<!-- /Reviews -->
				
				<!-- Clients -->
				<div class="clients">
					<div class="container">
						
						<?php include_once('workwithus.php'); ?>
						
					</div>
				</div>
				<!-- /Clients -->
				
				<!-- Map -->
				<?php include_once('map.php'); ?>
				<!-- /Map -->
				
			</div>
			<!-- /Content -->
			
		</div>
		<!-- /Wrapper -->
		
		<footer>
			<!-- Footer -->
			<div class="footer-wrapper">
				<div class="footer">
					<div class="container">
						
						<!-- Nav -->
						<?php include_once('navigator-bottom.php'); ?>
						<!-- /Nav -->
						
					</div>
				</div>
			</div>
			<!-- /Footer -->
			
			<!-- Callback Popup -->
			<?php include_once('callbackwnd.php'); ?>
			<!-- /Callback Popup -->
		</footer>
		
		
	<script type="application/ld+json">
		{
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [
				{
					"@type": "ListItem",
					"position": 1,
					"item": {
							"@id": "https://rocket4app.ru",
							"name": "Rocket4App",
							"image": "https://img.rocket4app.ru/images/maintenance_rocket.png"
					}
				},
				{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.ru/promotion.php",
						"name": "Раскрутка приложений",
						"image": "https://img.rocket4app.ru/images/advantages.png"
					}
				}
			]
        }
	</script>
	</body>
</html>
