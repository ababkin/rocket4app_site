<?php
function cases_row_print(&$openrow, &$totalcases, $totalinrow) {
	echo '<!-- openrow=' . $openrow . ', total=' . $totalcases . '-->';
	if ($openrow % $totalinrow == 0) {
		if ($openrow != 0)
			echo '</div><!-- close row -->';
		if ($totalcases != 0)
			echo '<div class="cases_row"><!-- open row -->';
	}
	++ $openrow;
	-- $totalcases;
}

$lastmod_day = 27;
$lastmod_month = 1;
$lastmod_year = 2017;
$lastmod_min = 33;
$lastmod_hour = 33;

include ("redirect.php");

$canonical = "//rocket4app.ru/cases.php";
$alternateEn = "//rocket4app.com/cases.php";
?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta name="referrer" content="origin">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

        <?php
								$page_data = array (
										"title" => "Продвижение мобильных приложений - Кейсы Rocket4App",
										"description" => "Кейсы компании Rocket4App - лидера в продвижении приложений для Google Play & AppStore",
										"h1" => "Кейсы"
								);
								?>

		<title><?php echo $page_data["title"]; //Кейсы | Rocket4App	  ?></title>
<meta name="description"
	content="<?php echo $page_data["description"]; ?>" />
		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate"
	hreflang="en" href="<?php echo $alternateEn; ?>" /><?php endif; ?>

		<link rel="shortcut icon"
	href="//<?php echo $_SERVER['SERVER_NAME'];?>/favicon.ico">

<style>
.case_archive:before {
	display: inline-block;
	content: "";
	background: url(/images/archive.png);
	width: 80px;
	height: 72px;
	position: absolute;
	z-index: 100;
	margin-left: -13px;
	margin-top: -11px;
	opacity: 1;
}

.cases_cell.case_archive {
	opacity: 0.7;
	display: none;
	#
	не
	показывать
	нафик
	старые
	мертвые
	кейсы
}

.cases .item_image {
	width: 130px !important;
}

.cases .item_image img, .case_img {
	width: 130px !important;
}

.cases_cell+.cases_cell {
	padding-left: 30px !important;
}

.cases_cell {
	width: 46% !important;
}

.item_text {
	margin-left: -10px !important;
}

.item_title {
	margin-left: -30px !important;
}

.cases .item_subtitle {
	background: transparent
		url("//img.rocket4app.ru/images/cases_title-new.png") no-repeat scroll
		0% 0%/100% 100% !important;
	padding: 0px 0px 0px 20px !important;
	margin-bottom: 0px !important;
	margin-left: -55px !important;
}

.item_flag {
	width: 57px;
	padding: 0px;
	height: 34px;
	margin: 0px;
}

.flag_usa {
	background: transparent
		url("//img.rocket4app.ru/images/flags/usa-i.png") no-repeat scroll 0%
		0%;
}

.flag_rus {
	background: transparent
		url("//img.rocket4app.ru/images/flags/rus-i.png") no-repeat scroll 0%
		0%;
}

.flag_nl {
	background: transparent url("//img.rocket4app.ru/images/flags/nl-i.png")
		no-repeat scroll 0% 0%;
}

.flag_uk {
	background: transparent url("//img.rocket4app.ru/images/flags/uk-i.png")
		no-repeat scroll 0% 0%;
}

.flag_hk {
	background: transparent url("//img.rocket4app.ru/images/flags/hk-i.png")
		no-repeat scroll 0% 0%;
}

.flag_fr {
	background: transparent url("//img.rocket4app.ru/images/flags/fr-i.png")
		no-repeat scroll 0% 0%;
}

.item_category {
	height: 34px !important;
}

.arrow {
	float: right;
	width: 18px;
	height: 34px;
	margin-top: -34px;
	background: transparent
		url("//img.rocket4app.ru/images/flags/arrow.png") no-repeat scroll 0px
		50%;
}

.item_description {
	margin: 3px 0px 6px -35px;
}

.cases_cell {
	padding-right: 0px !important;
}

.cases_cell>.in {
	padding-left: 200px !important;
}

.content.content_cases {
	padding: 0;
}
</style>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

	<?php
				// cases data

				$cases_json = '{
		{
		"title": "Naughty Little Dragons",
		"img": "//img.rocket4app.ru/images/cases/DragonPuzzleMatch3.png",
		"link": "case-dragon-puzzle-match3.php", //"https://www.appannie.com/apps/google-play/app/com.lynxar.nld/rank-history/#vtype=day&countries=US&start=2015-09-04&end=2015-09-07&view=rank&lm=1",
		"subtitle": [
		{
		"flag": "flag_usa",
		"category": "США: ТОП-50 Пазлы",
		"description": "Покупка инсталлов."
		},
		{
		"flag": "flag_rus",
		"category": "Россия: ТОП-10",
		"description": "Покупка инсталлов."
		}
		],

		}
		}';
				$cases = json_decode ( $cases_json );
				?>
	<body>

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
					<?php include_once('navigator-top.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content content_cases">

			<!-- Page Header -->
			<div class="page-header">
				<div class="container">
					<div class="in">

						<h1 class="page-header_title"><?php echo $page_data["h1"]; // Кейсы	   ?></h1>

						<div class="divider"></div>

					</div>
				</div>
			</div>
			<!-- /Page Header -->

			<!-- Cases -->
			<div class="cases">
				<div class="container">

					<div class="cases_intro">
						<p>
							<strong>Мы можем вывести в топ абсолютно любую игру или
								приложение. А также продержать его в топе столько, сколько будет
								выгодно клиенту. Оптимальный срок пребывания в топе, среднее
								количество дневных установок, доход с одной установки -
								индивиндуальны для каждого приложения или игры. Подобрать
								оптимальную стратегию вывода и удержания в топе именно для
								Вашего приложения Вам всегда помогут наши сотрудники.</strong>
						</p>
					</div>

					<!-- Cases List -->
					<?php
						$openrow = 0;
						$totalinrow = 2;
						$totalcases = 12;
					?>
					<div class="cases_list">

					<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>



				<!--Astro Boy-->
				<div class="cases_cell">
				<div class="in">
				<a href="cases/case-astro-boy.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	   ?>
				<div class="item_image">
				<img class="case_img"
				src="//img.rocket4app.com/images/cases/AstroBoy.png"
				<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
				alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
				</div>

				<div class="item_text">

				<h2 class="item_title">Astro Boy</h2>
				<h3 class="item_subtitle">
				<span class="item_flag flag_usa"></span> <span
				class="item_category">Топ-5 Новых в США</span> <span
				class="arrow"></span>
				</h3>
				<div class="item_description">Вывод в топ.</div>

				</div> </a>
				</div>
				</div>

				<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


										<!-- Stickman jailbreak 3-->
				<div class="cases_cell">
				<div class="in">
				<a href="cases/case-stickman-jailbreak-3.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	  ?>

																<div class="item_image">
						<img class="case_img"
							src="//img.rocket4app.com/images/cases/StickmanJailbreak3.png"
							<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
							alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
					</div>

					<div class="item_text">

						<h2 class="item_title">Stickman побег из тюрьмы 3</h2>
						<h3 class="item_subtitle">
							<span class="item_flag flag_usa"></span> <span
								class="item_category">Сбор органики</span> <span
								class="arrow"></span>
						</h3>
						<div class="item_description">Собрать максимум органики.</div>

					</div> </a>
				</div>
				</div>

				<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


												<!-- War Tank Racing Online 3d-->
						<div class="cases_cell">
						<div class="in">
						<a href="cases/case-war-tank-racing-online3d.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	  ?>

																		<div class="item_image">
								<img class="case_img"
									src="//img.rocket4app.com/images/cases/WarTankRacingOnline3d.png"
									<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
									alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
							</div>

							<div class="item_text">

								<h2 class="item_title">Битва гоночных танков</h2>
								<h3 class="item_subtitle">
									<span class="item_flag flag_usa"></span> <span
										class="item_category">США: ТОП-10 Гонок</span> <span
										class="arrow"></span>
								</h3>
								<div class="item_description">Вывод в Топ-10 Гонок США.</div>

							</div> </a>
						</div>
						</div>

						<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

						<!-- ARK Survival Island Evolve 3d-->
				<div class="cases_cell">
				<div class="in">
				<a href="cases/case-ARK-survival-island-evolve-3d.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	   ?>

												<div class="item_image">
				<img class="case_img"
				src="//img.rocket4app.com/images/cases/ARKSurvivalIslandEvolve3d.png"
				<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
				alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
				</div>

				<div class="item_text">

				<h2 class="item_title">Выживание на острове: АРК</h2>
				<h3 class="item_subtitle">
				<span class="item_flag flag_usa"></span> <span
				class="item_category">Топ-10 Приключений США</span> <span
				class="arrow"></span>
				</h3>
				<div class="item_description">Вывод в Топ-10 Приключений США.</div>

				</div> </a>
				</div>
				</div>

				<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

				<!-- Battle of painters-->
			<div class="cases_cell">
			<div class="in">
			<a href="cases/case-battle-of-painters.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	?>

										<div class="item_image">
			<img class="case_img"
			src="//img.rocket4app.com/images/cases/BattleOfPainters.png"
			<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
			alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
			</div>

			<div class="item_text">

			<h2 class="item_title">Битва художников</h2>
			<h3 class="item_subtitle">
			<span class="item_flag flag_usa"></span> <span
			class="item_category">placeholder</span> <span
			class="arrow"></span>
			</h3>
			<div class="item_description">placeholder.</div>

			</div> </a>
			</div>
			</div>

			<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

									<!--Chase Runner Patrol-->
						<div class="cases_cell">
						<div class="in">
						<a href="cases/case-chase-runner-patrol.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	 ?>
								<div class="item_image">
						<img class="case_img"
						src="//img.rocket4app.com/images/cases/ChaseRunnerPatrol.png"
						<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
						alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
						</div>

						<div class="item_text">

						<h2 class="item_title">Chase Runner Patrol</h2>
						<h3 class="item_subtitle">
						<span class="item_flag flag_usa"></span> <span
						class="item_category">Топ-2 новых Аркад</span> <span
						class="arrow"></span>
						</h3>
						<div class="item_description">Вывод в Топ-2 новых аркад США.</div>

						</div> </a>
						</div>
						</div>

						<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


									<!--Funny jumping-->
						<div class="cases_cell">
						<div class="in">
						<a href="cases/case-funny-jumping.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	   ?>
								<div class="item_image">
						<img class="case_img"
						src="//img.rocket4app.com/images/cases/FunnyJumping.png"
						<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
						alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
						</div>

						<div class="item_text">

						<h2 class="item_title">Funny jumping</h2>
						<h3 class="item_subtitle">
						<span class="item_flag flag_usa"></span> <span
						class="item_category">placeholder</span> <span
						class="arrow"></span>
						</h3>
						<div class="item_description">placeholder.</div>

						</div> </a>
						</div>
						</div>

						<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

					<!--Angry User-->
					<div class="cases_cell">
					<div class="in">
					<a href="cases/case-angry-user.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	?>
					<div class="item_image">
					<img class="case_img"
					src="//img.rocket4app.com/images/cases/AngryUser.png"
					<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
					alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
					</div>

					<div class="item_text">

					<h2 class="item_title">Angry User</h2>
					<h3 class="item_subtitle">
					<span class="item_flag flag_usa"></span> <span
					class="item_category">placeholder</span> <span
					class="arrow"></span>
					</h3>
					<div class="item_description">placeholder.</div>

					</div> </a>
					</div>
					</div>

					<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


															<!--Stickman bunker-->
											<div class="cases_cell">
											<div class="in">
											<a href="cases/case-stickman-bunker.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	 ?>
														<div class="item_image">
											<img class="case_img"
											src="//img.rocket4app.com/images/cases/StickmanBunker.png"
											<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
											alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
											</div>

											<div class="item_text">

											<h2 class="item_title">Stickman Бункер</h2>
											<h3 class="item_subtitle">
											<span class="item_flag flag_usa"></span> <span
											class="item_category">placeholder</span> <span
											class="arrow"></span>
											</h3>
											<div class="item_description">placeholder.</div>

											</div> </a>
											</div>
											</div>

											<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

											<!--Match 3 Amazon-->
									<div class="cases_cell">
									<div class="in">
									<a href="cases/case-match-3-amazon.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	?>
										<div class="item_image">
									<img class="case_img"
									src="//img.rocket4app.com/images/cases/Match3Amazon.png"
									<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
									alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
									</div>

									<div class="item_text">

									<h2 class="item_title">Амазонка: Три в ряд</h2>
									<h3 class="item_subtitle">
									<span class="item_flag flag_usa"></span> <span
									class="item_category">placeholder</span> <span
									class="arrow"></span>
									</h3>
									<div class="item_description">placeholder.</div>

									</div> </a>
									</div>
									</div>

									<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

									<!--Candy Blaze Mania-->
								<div class="cases_cell">
								<div class="in">
								<a href="cases/case-candy-blaze-mania.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	   ?>
								<div class="item_image">
								<img class="case_img"
								src="//img.rocket4app.com/images/cases/CandyBlazeMania.png"
								<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
								alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
								</div>

								<div class="item_text">

								<h2 class="item_title">Candy Blaze Mania</h2>
								<h3 class="item_subtitle">
								<span class="item_flag flag_usa"></span> <span
								class="item_category">placeholder</span> <span
								class="arrow"></span>
								</h3>
								<div class="item_description">placeholder.</div>

								</div> </a>
								</div>
								</div>

								<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


								<!--Primal Hunter Tribal Age-->
							<div class="cases_cell">
							<div class="in">
							<a href="cases/case-primal-hunter-tribal-age.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	  ?>
							<div class="item_image">
							<img class="case_img"
							src="//img.rocket4app.com/images/cases/PrimalHunterTribalAge.png"
							<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
							alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
							</div>

							<div class="item_text">

							<h2 class="item_title">Primal Hunter: Tribal Age</h2>
							<h3 class="item_subtitle">
							<span class="item_flag flag_usa"></span> <span
							class="item_category">placeholder</span> <span
							class="arrow"></span>
							</h3>
							<div class="item_description">placeholder.</div>

							</div> </a>
							</div>
							</div>

							<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


														<!-- Try to escape 2 -->
													<div class="cases_cell">
														<div class="in">
															<a href="cases/case-try-to-escape2.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	?>

																	<div class="item_image">
																	<img class="case_img"
																		src="//img.rocket4app.ru/images/cases/TryToEscape2.png"
																		<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
																		alt="Кейс: Голландия Вывод в ТОП-5 всех игр - Try to escape 2">
																</div>

																<div class="item_text">

																	<h2 class="item_title">Try to escape 2</h2>
																	<h3 class="item_subtitle">
																		<span class="item_flag flag_nl"></span> <span
																			class="item_category">Голландия: ТОП-1 Игр</span> <span
																			class="arrow"></span>
																	</h3>
																	<div class="item_description">Вывод в топ.</div>

																</div> </a>
														</div>
													</div>

							<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


														<!-- Дом 23 -->
													<div class="cases_cell">
														<div class="in">
															<!--a target="_blank"  rel="nofollow"
																href="https://www.appannie.com/apps/google-play/app/air.home23escape/rank-history/#vtype=day&countries=GB&start=2015-09-01&end=2015-09-07&view=rank&lm=7"-->

															<div class="item_image">
																<img class="case_img"
																	src="//img.rocket4app.ru/images/cases/WasWareWen.png"
																	<?php //https://lh3.googleusercontent.com/HpaSqS4sdMHHOL5JBf8atw0-XZEusCl3ucD2yJfW_sCkE0v8g3BSWCU_KiVMQpwffA=w300-rw"	  ?>
																	alt="Кейс: Was wäre wenn? Австрия, Испания">
															</div>

															<div class="item_text">

																<h2 class="item_title">Was wäre wenn?</h2>
																<a href="cases/case-was-ware-wenn-at.php">
																	<h3 class="item_subtitle">
																		<span class="item_flag flag_uk"></span> <span
																			class="item_category">Австрия: Топ-1 Игр</span> <span
																			class="arrow"></span>
																	</h3>
																	<div class="item_description">Вывод в топ.</div>
																</a> <a href="cases/case-was-ware-wenn-es.php">
																	<h3 class="item_subtitle">
																		<span class="item_flag flag_rus"></span> <span
																			class="item_category">Испания: ТОП-10 Казуальных</span> <span
																			class="arrow"></span>
																	</h3>
																	<div class="item_description">Вывод в топ.</div>
																</a>

															</div>
														</div>
													</div>

							<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


														<!-- Stickman jailbreak escape -->
													<div class="cases_cell">
														<div class="in">
															<a href="cases/case-stickman-jailbreak-escape.php">
							<?php // href="https://www.appannie.com/apps/google-play/app/justmoby.justmusic.player/rank-history/#vtype=day&countries=US&start=2016-03-13&end=2016-04-11&view=rank&lm=7">   ?>

																	<div class="item_image">
																	<img class="case_img"
																		src="//img.rocket4app.ru/images/cases/StickmanJailbreak.png"
																		<?php
																		// https://lh3.googleusercontent.com/s7WNJDAEYMwOHzHP_dxu13QWai1elnUKe5pVizCXxK6WHwp8iA2rkkOtp9cT9JPUBtI=w300-rw"
																		?>
																		alt="Кейс: Сбор дополнительной органики">
																</div>
																<div class="item_text">

																	<h2 class="item_title">Stickman jailbreak escape</h2>
																	<h3 class="item_subtitle">
																		<span class="item_flag flag_usa"></span> <span
																			class="item_category">Увеличить органику</span> <span
																			class="arrow"></span>
																	</h3>
																	<div class="item_description">Сбор дополнительной органики.</div>
																</div>
															</a>
														</div>
													</div>

							<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


														<!-- Спасение Люси -->
													<div class="cases_cell">
														<div class="in">
															<a href="cases/case-lucy.php">
							<?php // href="https://www.appannie.com/apps/google-play/app/justmoby.justmusic.player/rank-history/#vtype=day&countries=US&start=2016-03-13&end=2016-04-11&view=rank&lm=7">   ?>

																	<div class="item_image">
																	<img class="case_img"
																		src="//img.rocket4app.ru/images/cases/Lucy.png"
																		<?php
																		// https://lh3.googleusercontent.com/s7WNJDAEYMwOHzHP_dxu13QWai1elnUKe5pVizCXxK6WHwp8iA2rkkOtp9cT9JPUBtI=w300-rw"
																		?>
																		alt="Кейс: Сбор органики с разных стран (Россия, Германия, США)">
																</div>
																<div class="item_text">

																	<h2 class="item_title">Спасение Люси</h2>
																	<h3 class="item_subtitle">
																		<span class="item_flag flag_usa"></span> <span
																			class="item_category">Сбор органики</span> <span
																			class="arrow"></span>
																	</h3>
																	<div class="item_description">Собрать максимум органики.</div>
																</div>
															</a>
														</div>
													</div>

							<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


														<!-- Adventure paw ninja patrol -->
													<div class="cases_cell">
														<div class="in">
															<a href="cases/case-adventure-paw-ninja.php">
							<?php // href="https://www.appannie.com/apps/google-play/app/justmoby.justmusic.player/rank-history/#vtype=day&countries=US&start=2016-03-13&end=2016-04-11&view=rank&lm=7">   ?>

																	<div class="item_image">
																	<img class="case_img"
																		src="//img.rocket4app.ru/images/cases/AdventureDogPatrolPawNinja.png"
																		<?php
																		// https://lh3.googleusercontent.com/s7WNJDAEYMwOHzHP_dxu13QWai1elnUKe5pVizCXxK6WHwp8iA2rkkOtp9cT9JPUBtI=w300-rw"
																		?>
																		alt="Кейс: Максимум органики с минимальным вложением">
																</div>
																<div class="item_text">

																	<h2 class="item_title">Adventure paw ninja patrol</h2>
																	<h3 class="item_subtitle">
																		<span class="item_flag flag_usa"></span> <span
																			class="item_category">Сбор органики</span> <span
																			class="arrow"></span>
																	</h3>
																	<div class="item_description">Собрать максимум органики.</div>
																</div>
															</a>
														</div>
													</div>

							<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

														<!-- Дом страха - Тюрьма -->
													<div class="cases_cell">
														<div class="in">
															<a href="cases/case-lucy-escape.php">
							<?php // href="https://www.appannie.com/apps/google-play/app/justmoby.justmusic.player/rank-history/#vtype=day&countries=US&start=2016-03-13&end=2016-04-11&view=rank&lm=7">   ?>

																	<div class="item_image">
																	<img class="case_img"
																		src="//img.rocket4app.ru/images/cases/Lucy-escape.png"
																		<?php
																		// https://lh3.googleusercontent.com/s7WNJDAEYMwOHzHP_dxu13QWai1elnUKe5pVizCXxK6WHwp8iA2rkkOtp9cT9JPUBtI=w300-rw"
																		?>
																		alt="Кейс: США Вывод в ТОП-1 по запросу - can you escape">
																</div>
																<div class="item_text">

																	<h2 class="item_title">Дом страха - Тюрьма</h2>
																	<h3 class="item_subtitle">
																		<span class="item_flag flag_usa"></span> <span
																			class="item_category">США: ТОП-1 Поиск</span> <span
																			class="arrow"></span>
																	</h3>
																	<div class="item_description">Вывод в топ по запросу.</div>
																</div>
															</a>
														</div>
													</div>

							<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>




							<!-- Ферма -->
						<div class="cases_cell">
							<div class="in">
								<a href="cases/case-farm.php">
<?php
// target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/com.foranj.farmtown/keywords/#countries=US&start=2016-01-12&end=2016-04-10">
?>

										<div class="item_image">
										<img class="case_img"
											src="//img.rocket4app.ru/images/cases/Farm.png"
											<?php //https://lh3.googleusercontent.com/oxWkTj_30D9SFjEtaulOl5MLegHw6RpLef875gXLLpoPfR36iubtQQOINkuQPh_RMJiN=w300-rw"	   ?>
											alt="Кейс: США ТОП-1 по запросу - farm">
									</div>

									<div class="item_text">

										<h2 class="item_title">Ферма Бесплатно с Барашками</h2>
										<h3 class="item_subtitle">
											<span class="item_flag flag_usa"></span> <span
												class="item_category">США: ТОП-1 Поиск</span> <span
												class="arrow"></span>
										</h3>
										<div class="item_description">Вывод в топ по запросу.</div>


									</div>
								</a>
							</div>
						</div>

<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

							<!-- Покер -->
						<div class="cases_cell">
							<div class="in">
								<a href="cases/case-newpoker.php">
<?php // href="https://www.appannie.com/apps/google-play/app/com.lynxar.nld/rank-history/#vtype=day&countries=US&start=2015-09-04&end=2015-09-07&view=rank&lm=1">	?>

										<div class="item_image">
										<img class="case_img"
											src="//img.rocket4app.ru/images/cases/NewPoker.png"
											<?php
											// https://lh3.googleusercontent.com/s7WNJDAEYMwOHzHP_dxu13QWai1elnUKe5pVizCXxK6WHwp8iA2rkkOtp9cT9JPUBtI=w300-rw"
											?>
											alt="Топ-5 по запросу 'poker' в США">
									</div>

									<div class="item_text">

										<h2 class="item_title">Покер</h2>
										<h3 class="item_subtitle">
											<span class="item_flag flag_usa"></span> <span
												class="item_category">США: ТОП-5 Поиск</span> <span
												class="arrow"></span>
										</h3>
										<div class="item_description">Вывод в топ по запросу.</div>

									</div>
								</a>
							</div>
						</div>

<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

							<!-- Дом 23 -->
						<div class="cases_cell">
							<div class="in">
								<!--a target="_blank"  rel="nofollow"
									href="https://www.appannie.com/apps/google-play/app/air.home23escape/rank-history/#vtype=day&countries=GB&start=2015-09-01&end=2015-09-07&view=rank&lm=7"-->

								<div class="item_image">
									<img class="case_img"
										src="//img.rocket4app.ru/images/cases/House23Escape.png"
										<?php //https://lh3.googleusercontent.com/HpaSqS4sdMHHOL5JBf8atw0-XZEusCl3ucD2yJfW_sCkE0v8g3BSWCU_KiVMQpwffA=w300-rw"	  ?>
										alt="Кейс: Англия Вывод в ТОП-3 Приключения - Дом 23 - Побег">
								</div>

								<div class="item_text">

									<h2 class="item_title">Дом 23 - Побег</h2>
									<a href="cases/case-house-23-uk.php">
										<h3 class="item_subtitle">
											<span class="item_flag flag_uk"></span> <span
												class="item_category">Англия: ТОП-3 Приключений</span> <span
												class="arrow"></span>
										</h3>
										<div class="item_description">Вывод в топ.</div>
									</a> <a href="cases/case-house-23-rus.php">
										<h3 class="item_subtitle">
											<span class="item_flag flag_rus"></span> <span
												class="item_category">Россия: ТОП-15 Приключений</span> <span
												class="arrow"></span>
										</h3>
										<div class="item_description">Вывод в топ.</div>
									</a>

								</div>
							</div>
						</div>

<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>

							<!-- Escape - fear house -->
						<div class="cases_cell">
							<div class="in">
								<a href="cases/case-escape-fear-house.php"><?php //target="_blank" rel="nofollow" href="https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-03&end=2015-09-06&view=rank&lm=3">	   ?>

										<div class="item_image">
										<img class="case_img"
											src="//img.rocket4app.ru/images/cases/EscapeFearHouse.png"
											<?php //https://lh3.googleusercontent.com/SjV-ZFtX0SqaUG4CrBUYGeZOiPvP4xG6S5GRsJD862_9EgDh242V_NJ49cLWoay3ng=w300-rw"	  ?>
											alt="Кейс: США Вывод в ТОП-30 Пазлы - Escape - fear house">
									</div>

									<div class="item_text">

										<h2 class="item_title">Дом страха - Побег</h2>
										<h3 class="item_subtitle">
											<span class="item_flag flag_usa"></span> <span
												class="item_category">США: ТОП-10 Пазлов</span> <span
												class="arrow"></span>
										</h3>
										<div class="item_description">Вывод в топ.</div>

									</div> </a>
							</div>
						</div>

						<?php cases_row_print($openrow, $totalcases, $totalinrow); ?>


						</div>

					<!-- /Cases List -->

				</div>
			</div>
			<!-- /Cases -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">

<?php include_once('sendform-yellow.php'); ?>

					</div>
			</div>
			<!-- /Apply -->

			<!-- Reviews -->
			<div class="reviews inner_shadow __light">
				<div class="container">

<?php include_once('reviews.php'); ?>

					</div>
			</div>
			<!-- /Reviews -->

			<!-- Advantages -->
			<div class="advantages inner_shadow __white">
				<div class="container">

<?php include_once('advantages.php'); ?>

					</div>
			</div>
			<!-- Advantages -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
<?php include_once('navigator-bottom.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<!-- Callback Popup -->
<?php include_once('callbackwnd.php'); ?>
		<!-- /Callback Popup -->

	<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<script type="application/ld+json">
            {
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "item": {
            "@id": "https://rocket4app.ru",
            "name": "Rocket4App",
			"image": "https://img.rocket4app.ru/images/maintenance_rocket.png"
            }
            },{
            "@type": "ListItem",
            "position": 2,
            "item": {
            "@id": "https://rocket4app.ru/cases.php",
            "name": "Наши кейсы",
			"image": "https://img.rocket4app.ru/images/maintenance_rocket.png"
            }
            }]
            }
		</script>
</body>
</html>
