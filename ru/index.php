<?php
	$lastmod_day = 20;
	$lastmod_month = 8;
	$lastmod_year = 2017;
	$lastmod_min = 2;
	$lastmod_hour = 1;
	
	include ("redirect.php");
	
	
	$hostname = $_SERVER ['SERVER_NAME'];
	$hostname = substr ( $hostname, 0, strpos ( $hostname, ".ru" ) );
	
	$canonical = "//rocket4app.ru";
	$alternateEn = "//rocket4app.com";
	
	$og_site_name = "Rocket4App";
	$og_title = "Rocket4App - продвижение мобильных приложений и игр";
	$og_image = "//img.rocket4app.ru/images/logo.jpg";
	$og_description = "Бесплатный вывод приложений в ТОП.";
	$og_url = $canonical;
	
	$page_data = array (
	"title" => "Продвижение мобильных приложений Android в Google Play и IOS в AppStore, раскрутка мобильных приложений и вывод в топ",
	// "description" => "Закажите продвижение мобильных приложений Android Андроид в Google Play и IOS в AppStore от профессионалов. Раскрутку мобильных приложений и вывод в топ Вы можете заказать уже сейчас на нашем сайте",
	"description" => "Постоплата при продвижении мобильных игр и приложений от профессионалов с огромным опытом. Оплата по факту! | Rocket4App",
	"h1" => "<h1 class='header_info_title' style='width: 400px;'>Продвижение мобильных приложений в топ Google Play и App Store</h1>",
	// "h1" => "<h1 class='header_info_title' style='width: 390px;'>Продвижение мобильного приложения <small>в Топ Google Play и AppStore</small></h1>" //"Продвижение Вашего мобильного приложения в топ Google Play и AppStore"
	"h2" => array (
	"0" => "<h2>Продвижение мобильного приложения наш конек!<h2>",
	"1" => "Оставить заявку <br>на продвижение <br><small style='padding-top:10px'>приложения или игры</small>",
	"2" => "Написать <strong>хорошее приложение</strong> – лишь половина успеха.<br> Вторая	половина – <strong>успешное продвижение приложения!</strong>",
	"3" => "Почему стоит<br> продвигать приложение с нами:" 
	) 
	);
	
	$yellow_title = "Заказать продвижение приложений прямо сейчас";
?>

<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="referrer" content="origin">
		<meta name="viewport" content="width=1000">
		
		<?php //<meta http-equiv="X-UA-Compatible" content="IE=edge chrome=1"> ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<!-- SEO Tags -->
		<title><?php echo $page_data["title"]; //Главная | Rocket4App ?></title>
		<meta name="description"
		content="<?php echo $page_data["description"]; ?>">
		<meta name="keywords"
		content="продвижение приложений  продвижение мобильных приложений  продвижение приложений в appstore  продвижение андроид приложений  продвижение android приложений  продвижение приложений в google play  продвижение ios приложения  продвижение мобильных приложений кейсы  как продвигать мобильные приложения  как продвигать приложение  раскрутка приложений  раскрутка мобильных приложений  раскрутка приложений android  раскрутка приложений в google play  раскрутка приложений ios  раскрутка андроид приложений  раскрутка приложений в appstore  вывод в топ appstore" />
		<meta name="mailru-verification" content="4d1ddbccaa6f9862" />
		<!-- /SEO Tags -->
		<!-- OG Tags -->
		<meta http-equiv="content-language" content="ru">
		<meta property="og:site_name" content="<?php echo $og_site_name; ?>" />
		<meta property="og:title" content="<?php echo $og_title; ?>" />
		<meta property="og:image" content="<?php echo $og_image; ?>" />
		<meta property="og:description" content="<?php echo $og_description; ?>" />
		<meta property="og:url" content="<?php echo $og_url; ?>" />
		<meta property="og:type" content="website" />
		<!-- /OG Tags -->
		
		<link rel="shortcut icon" href="/favicon.ico">
		<?php if (isset($canonical)): ?><link rel="canonical"
		href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="en"
		href="<?php echo $alternateEn; ?>" /><?php endif; ?>
		
		<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
		<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
		<script type="text/javascript" src="js/owl.carousel.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		
		<style>
			.why_list a, .und {
			text-decoration: underline;
			}
		</style>
	</head>
	<body class="homepage">
		
		<!-- Wrapper -->
		<div class="wrapper">
			
			<!-- Header -->
			<section>
				<div class="header">
					<div class="container">
						
						<!-- Nav -->
						<?php include_once('navigator-top.php'); ?>
						<!-- /Nav -->
						
						<link rel="stylesheet" type="text/css" href="/css/style.css">
						
						<!-- Header Info -->
						<div class="header_info">
							<div class="container">
								
								<div class="header_info_left">
									<?php
										// <h1 class="header_info_title">
										// Поднимем Ваше<br> мобильное приложение<br> <small>в Топ AppStore<br>
										// и Google Play
										// </small>
										// </h1>
										echo $page_data ["h1"];
									?>
									
									<a href="#about-top" class="btn btn_white"><span
									class="icon-more"></span>Узнайте как</a>
									
								</div>
								
								<div class="header_info_right">
									
									<div class="header_info_form">
										
										<h2 class="header_info_form_title">
											<?php
												// Оставить заявку <small>на продвижение</small>
												echo $page_data ["h2"] ["1"];
											?>
										</h2>
										
										<form method="POST" action="mail.php"
										onsubmit="yaCounter44221814.reachGoal('FORM'); return true;">
											
											<div class="header_info_form_controls">
												<div class="form-group has-icon">
													<input type="text" name="name" class="form-control __no-bg"
													placeholder="Ваше имя"> <span
													class="form-control-icon icon-user-white"></span>
												</div>
												<div class="form-group has-icon">
													<input type="text" name="phone" class="form-control __no-bg"
													placeholder="Ваш телефон"> <span
													class="form-control-icon icon-phone-white"></span>
												</div>
												<div class="form-group has-icon">
													<input type="text" name="email" class="form-control __no-bg"
													placeholder="или Ваш e-mail" required> <span
													class="form-control-icon icon-envelope-white"></span>
												</div>
											</div>
											
											<div class="form-group form-group-button">
												<button type="submit" name="submit" class="btn btn_apply">
													<span class="icon-apply"></span>Отправить заявку
												</button>
											</div>
											
										</form>
										
									</div>
									
								</div>
								
							</div>
						</div>
						<!-- -->
						
					</div>
				</div>
			</section>
			<!-- /Header -->
			
			<!-- Content -->
			<div class="content">
				
				<!-- Seo -->
				<section>
					<div class="seo" style="height: 740px">
						<div class="container">
							<div class="in" style="padding-top: 20px">
								
								<div class="seo_arrow">
									<span class="icon-arrow-down"></span>
								</div>
								
								<a id="about-top"></a>
								<h1 class="seo_title" style="width: 600px; margin-left: 180px;">
									Продвижение мобильного приложения в топ Google Play и App Store
								</h1>
								
								<div class="divider"></div>
								
								<div class="seo_text" style="text-align: left; font-size: 14px">
									<h2 class="seo_title" style="text-align:left">    Кто мы?</h2>
									
									<br>
 
									<p>Мы небольшая команда увлеченных молодых людей, прошедших долгий путь 
									от продвижения своего приложения до тысячи успешных проектов сторонних 
									разработчиков. Строго следуя рекомендациям Google Play и App Store, мы 
									нащупали баланс потребительских качеств приложения, благодаря которым 
									приложение быстро становится популярным и в короткие сроки попадает в 
									топ сторов.</p>
									
									<br>
									
									<p>С нашей помощью ваши приложения будут отвечать самым требовательным 
									запросам как конечных пользователей, так и магазинов приложений, что 
									позволит продвигаться приложениям к топу, а вам увеличивать продажи и 
									доходы.</p>
    
									<br>
									
									<h2 class="seo_title" style="text-align:left">
										Зачем необходимо продвижение приложения?
									</h2>
 
									<br>
									
									<p>На рынок выпущено тысячи приложений, как очень похожих друг на друга, 
									так и совершенно не похожих, но при этом никому неизвестных. Как показывает 
									статистика и опыт, чтобы приложение набирало популярность, необходимо 
									попасть в верхние позиции магазинов приложений. Но ни один магазин не 
									предлагает готового решения. Есть рекомендации чего придерживаться и что 
									категорически не надо делать. И в каждой нише свои нюансы, которые нельзя 
									не учитывать! Простому разработчику нащупать этот путь стоит кучи времени 
									и немалых денег.  </p>
									
									<br>
									
									<p>Необходимо грамотно подготовить приложение к публикации в сторе, 
									оптимизировать метаинформацию, разработать маркетинговый план и привлечь 
									сотни и тысячи конечных пользователей к вашему приложению. Тем самым дать 
									мощный толчок росту узнаваемости приложения, количеству установок, 
									расширению комьюнити и как следствие – стремительному взлету вверх 
									финансовых показателей!</p>
								</div>
								
							</div>
						</div>
					</div>
				</section>
				<!-- /Seo -->
				
				<!-- How it works -->
				<section>
					<div class="how-it-works">
						<div class="container">
							
							<h2 class="how-it-works_title section_title">
								Сделаем ваше приложение самым заметным!<br> 
								<small>оптимизация - путь к успеху!
								</small>
							</h2>
							
							<div class="how-it-works_scheme">
								<img src="//img.rocket4app.ru/images/scheme.png" alt="Как это работает">
							</div>
							
							<div class="how-it-works_text seo_text" style="text-align: left;">
								<h1 style="font-weight: 600;font-size: 160%;margin-bottom: 25px;">
									Продвижение мобильных приложений Андроид в Google Play и IOS в 
									AppStore с компанией Rocket4App
								</h1>
								
								<p>В вопросе вывода приложения в топ имеют значение следующие 
								факторы:</p>
								
								<ul>
									<li>установки приложений в первый день публикации</li>
									<li>оптимизированное и выделяющееся описание приложения</li>
									<li>ASO оптимизация для интуитивного нахождения приложения 
									в поиске</li>
									<li>оценки пользователей</li>
								</ul>

								<p>Чему еще уделить внимание для достижения требуемых показателей 
								в перечисленных выше пунктах:</p>

								<ul>
									<li>реклама </li>
									<li>вовлечение пользователя в игровой процесс     </li>
									<li>расширение сообщества лояльных пользователей    </li>
									<li>привлечение пользователей в медиа    </li>
									<li>кросс-промоушн    </li>
									<li>публикация информации в СМИ и на профильных форумах    </li>
									<li>популяризация через видеопрохождения</li>
								</ul>

								<p>Все это помогает добиться необходимого числа установок, хороших 
								оценок и роста популярности продукта, что непременно продвигает 
								приложение в топ Google Play и App Store.    </p>
								
								<p>Заниматься всеми перечисленными направлениями разработчику 
								зачастую не под силу. Ему просто не останется времени на разработку 
								и выпуск новых версий.</p>
								
								<center>
									<img src="//img.rocket4app.ru/rocket4app-promotion-methods.gif" width="100%">
								</center>
                            </div>
							
						</div>
					</div>
				</section>
				<!-- How it works -->
				
				<!-- Advantages -->
				<div class="advantages inner_shadow">
					<div class="container">
						
						<?php include_once('advantages_promotion.php'); ?>
						
					</div>
				</div>
				<!-- Advantages -->
				
				<!-- Why -->
				<section>
					<div class="why inner_shadow">
						<div class="container" style="padding-left: 0px; padding-bottom: 0px; padding-top: 30px;">
							
							<a id="about-us"></a>
							<h2 class="why_title" style="text-align: right; width: 850px; margin-left: 50px ! important;">
								Rocket4App — отличное продвижение приложений за разумные деньги
							</h2>
							
							<ol class="why_list" style="padding-left:250px;background:transparent url(//img.rocket4app.ru/images/numbers1.png) no-repeat scroll 270px 40%;">
								<li>Мы зарабатываем за счет оборота. Поэтому расценки на продвижение 
								минимальны и гораздо ниже, чем на медийную рекламу, баннеры и платные 
								обзоры. Получите результат с минимальными финансовыми затратами!
								</li>
								<li>Мы уверены в своих силах и хорошо представляем себе тонкости 
								продвижения мобильных приложений. Поэтому и не боимся работать с 
								оплатой по факту. Так что будьте уверенны: вы не потеряете деньги 
								ни при каких обстоятельствах.	
								</li>
								<li>Наши методики соответствуют требованиям GooglePlay и AppStore! 
								Находясь в гуще событий мы знаем о  появляющихся нюансах и тенденциях,
								поэтому добиваемся результата!
								</li>
								<li>Уже 5 лет мы работаем на рынке продвижения приложений.Раньше — 
								каждый по отдельности, а теперь — в команде. Обращаясь к нам, вы в 
								короткие сроки получаете ценный органический трафик, а значит — прибыль!
								</li>
								<br>
								<li>Мы всегда на связи. В нашей команде всего шесть человек, зато все 
								внимание — вашему проекту и никакой бюрократии. Вы всегда найдете в 
								нашем лице товарища и советчика
								</li>
								<li>Наши результаты просто проверить. Для этого взгляните на наши 
								кейсы — прозрачно, четко и понятно. Ваши затраты на рекламу окупаются,
								а приложение начинает приносить прибыль. Конечно, если знать, как 
								действовать. Мы — знаем!	
								</li>
							</ol>
							
							<div class="why_rocket" style="bottom: 0px;"></div>
							
						</div>
						

						
					</div>
				</section>
				<!-- /Why -->
				
				<!-- Apply -->
				<div class="apply inner_shadow">
					<div class="container">
						
						<?php include_once('sendform-yellow.php'); ?>
						
					</div>
				</div>
				<!-- /Apply -->
				
				<!-- Reviews -->
				<!--div class="reviews inner_shadow">
					<div class="container">
						
						<?php include_once('reviews.php'); ?>
						
					</div>
				</div-->
				<!-- /Reviews -->
				
				<!-- Clients -->
				<div class="clients">
					<div class="container">
						
						<?php include_once('workwithus.php'); ?>
						
					</div>
				</div>
				<!-- /Clients -->
				
				<!-- Map -->
				<?php include_once('map.php'); ?>
				<!-- /Map -->
				
			</div>
			<!-- /Content -->
			
		</div>
		<!-- /Wrapper -->
		
		<footer>
			<!-- Footer -->
			<div class="footer-wrapper">
				<div class="footer">
					<div class="container">
						
						<!-- Nav -->
						<?php include_once('navigator-bottom.php'); ?>
						<!-- /Nav -->
						
					</div>
				</div>
			</div>
			<!-- /Footer -->
			
			<!-- Callback Popup -->
			<?php include_once('callbackwnd.php'); ?>
			<!-- /Callback Popup -->
		</footer>
		
		<script type="application/ld+json">
			{
				"@context": "http://schema.org", 
				"@type": "BreadcrumbList", 
				"itemListElement": [{
					"@type": "ListItem", 
					"position": 1, 
					"item": {
						"@id": "https://rocket4app.ru", 
						"name": "Rocket4App",
						"image": "https://img.rocket4app.ru/images/maintenance_rocket.png"
					}
					
				}]
			}
		</script>
		
	</body>
</html>
