<?php
$cases_list = array (
		array (
				"name" => "Дом страха - Тюрьма",
				"brand" => "//img.rocket4app.ru/images/cases/Lucy-escape.png" 
		),
		array (
				"name" => "Ферма",
				"brand" => "//img.rocket4app.ru/images/cases/Farm.png" 
		),
		array (
				"name" => "Дом 23",
				"brand" => "//img.rocket4app.ru/images/cases/House23Escape.png" 
		),
		array (
				"name" => "Покер",
				"brand" => "//img.rocket4app.ru/images/cases/NewPoker.png" 
		),
		array (
				"name" => "Дом страха - Побег",
				"brand" => "//img.rocket4app.ru/images/cases/EscapeFearHouse.png" 
		),
		array (
				"name" => "Dog Patrol Paw Ninja",
				"brand" => "//img.rocket4app.ru/images/cases/AdventureDogPatrolPawNinja.png" 
		),
		array (
				"name" => "Was wäre wenn?",
				"brand" => "//img.rocket4app.ru/images/cases/WasWareWen.png" 
		),
		array (
				"name" => "Try to escape 2",
				"brand" => "//img.rocket4app.ru/images/cases/TryToEscape2.png" 
		),
		array (
				"name" => "Stickman jailbreak escape",
				"brand" => "//img.rocket4app.ru/images/cases/StickmanJailbreak.png" 
		),
		array (
				"name" => "Спасение Люси",
				"brand" => "//img.rocket4app.ru/images/cases/Lucy.png" 
		) 
);

shuffle ( $cases_list );
?>

<h2 class="clients_title">С нами уже работают</h2>

<div class="clients_cases text-center">
	<a href="/cases.php" class="clients_cases_link">Кейсы</a>
</div>

<a href="/cases.php"
	title="Узнайте больше о наших кейсах! Мы продвинем и Ваше приложение!"
	style="text-decoration: none;">
	<ul class="clients_list">
	    <?php 		for ($i = 0; $i < 5; ++$i) {   ?>
		  <li><span class="item_image"><img
				src="<?php echo $cases_list[$i]["brand"]; ?>"
				alt="Наш кейс: <?php echo $cases_list[$i]["name"];?>"></span> <span
			class="item_title"><?php echo $cases_list[$i]["name"]; ?></span></li>
		<?php } ?>
	</ul>
</a>