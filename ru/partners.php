<?php
$lastmod_day = 18;
$lastmod_month = 4;
$lastmod_year = 2016;
$lastmod_min = 9;
$lastmod_hour = 2;
include ("redirect.php");

$canonical = "//rocket4app.ru/partners.php";
$alternateEn = "//rocket4app.com/partners.php";

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta name="referrer" content="origin">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Сотрудничество - продвижение ваших мобильных приложений|
	Rocket4App</title>
<meta name="description"
	content="Варианты сотрудничества для разработчиков и реселлеров в выводе приложений в топ с компанией Rocket4App">

<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="shortcut icon" href="/favicon.ico">

		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="en"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>


</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
				<?php include_once('navigator-top.php'); ?>
				<!-- /Nav -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Page Header -->
			<div class="page-header">
				<div class="container">
					<div class="in">

						<h1 class="page-header_title">Сотрудничество</h1>

						<div class="divider"></div>

					</div>
				</div>
			</div>
			<!-- /Page Header -->

			<!-- Cooperation -->
			<div class="cooperation">
				<div class="container">

					<div class="item post __reseller">

						<div class="item_image">
							<img src="//img.rocket4app.ru/images/content/reseller.png" alt="Реселлерам">
						</div>

						<h2 class="item_title section_title">
							Реселлерам<br> <small>Resellers</small>
						</h2>

						<p>
							<strong>Зарабатывайте вместе с нами и помогайте другим раскрутить
								их игры и приложения Google Play и AppStore!</strong>
						</p>

						<p>Предлагайте наши услуги своим клиентам и получайте % от их
							платежей!</p>
						<p>Чем больше клиентов вы будете находить, тем больше с каждого
							будете получать.</p>
						<p>Имейте ввиду, что наш средний чек - это 5000$, а хорошим
							реселлерам, мы готовы отдавать до 25%</p>


					</div>

					<div class="dashed_divider"></div>

					<div class="item post __developer">

						<div class="item_image">
							<img src="//img.rocket4app.ru/images/content/developer.png"
								alt="Разработчикам мобильных игр и приложений">
						</div>

						<h2 class="item_title section_title">
							Разработчикам<br> <small>Developers</small>
						</h2>

						<p>
							<strong>У вас есть интересные игры или приложения, но нет
								технологий или средств, что бы сделать игру известной? Не
								знаете, как достичь высоких рейтингов и попасть в
								ТОП-ы store-ов?</strong> 
						</p>

						<p>
							Мы предлагаем <a
								style="color: rgb(0, 161, 236); font-weight: 600;"
								href="/publisher/"
								title="Узнайте все о бесплатном продвижении мобильных приложений и игр">уникальную
								услугу Издателя</a>
						</p>
						<p>
							Издавайтесь с нами, рекламируйте свои игры бесплатно, попадайте в
							<a href="/googleplay/"
								title="Закажите вывод в топ вашего мобильного приложения">топ
								Google Play и AppStore!</a>
						</p>
						<p>Мы поможем вывести ваше приложение в Топы самых популярных
							стран мира за долю от будущей прибыли.</p>
						<p>Практикуем индивидуальный подход к каждому проекту.</p>

					</div>

				</div>
			</div>
			<!-- /Cooperation -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">
						
						<?php include_once('sendform-yellow.php'); ?>
						
					</div>
			</div>
			<!-- /Apply -->

			<!-- Clients -->
			<div class="clients inner_shadow">
				<div class="container">

					<?php include_once('workwithus.php'); ?>

				</div>
			</div>
			<!-- /Clients -->

			<!-- Map -->
			<?php include_once('map.php'); ?>
			<!-- /Map -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
                <?php include_once('navigator-bottom.php'); ?>
                <!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="/js/owl.carousel.js"></script>
	<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>

	<!-- Callback Popup -->
	<?php include_once('callbackwnd.php'); ?>
	<!-- /Callback Popup -->

	<script type="application/ld+json">
		{
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [
				{
					"@type": "ListItem",
					"position": 1,
					"item": {
							"@id": "https://rocket4app.ru",
							"name": "Rocket4App",
							"image": "https://img.rocket4app.ru/images/maintenance_rocket.png"
					}
				},
				{
					"@type": "ListItem",
					"position": 2,
					"item": {
							"@id": "https://rocket4app.ru/partners.php",
							"name": "Сотрудничество",
							"image": "https://img.rocket4app.ru/images/content/developer.png"
					}
				}
			]
        }
	</script>

</body>
</html>