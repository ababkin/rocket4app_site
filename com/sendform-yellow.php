<h2 class="apply_title">
	<?php
	if (isset ( $yellow_title ))
		echo $yellow_title;
	else
		echo "Оrder your app promotion services right now";
	?>
</h2>

<form id="send_me" class="apply_form" method="POST"
	action="//<?php echo $_SERVER['SERVER_NAME']; ?>/mail.php"
	onsubmit="yaCounter31274678.reachGoal('YELLOWFORM'); return true;">

	<div class="apply_form_controls maintenance_form_controls">
		<div class="form-group has-icon">
			<input type="text" name="name" class="form-control __no-border"
				placeholder="Your name"> <span class="form-control-icon icon-user"></span>
		</div>
		<div class="form-group has-icon">
			<input type="text" name="phone" class="form-control __no-border"
				placeholder="Your phone"> <span class="form-control-icon icon-phone"></span>
		</div>
		<div class="form-group has-icon">
			<input type="text" name="email" class="form-control __no-border"
				placeholder="Your e-mail" required> <span
				class="form-control-icon icon-envelope"></span>
		</div>
	</div>

	<div class="form-group form-group-button">
		<button type="submit" name="submit" class="btn btn_apply">
			<span class="icon-apply"></span>
				<?php
				if (isset ( $yellow_btn ))
					echo $yellow_btn;
				else
					echo "Send your order for app promotion";
				?>
		</button>
	</div>

</form>