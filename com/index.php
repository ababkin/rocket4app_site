<?php
$lastmod_day = 20;
$lastmod_month = 8;
$lastmod_year = 2017;
$lastmod_min = 2;
$lastmod_hour = 1;

include ("en/redirect.php");

$hostname = $_SERVER ['SERVER_NAME'];
$hostname = substr ( $hostname, 0, strpos ( $hostname, ".com" ) );

$canonical = "//rocket4app.com";
$alternateEn = "//rocket4app.ru";

$og_site_name = "Rocket4App";
$og_title = "Rocket4App - Promoting Android and iOS mobile apps on Google Play and the App Store, boosting mobile games into the top rankings";
$og_image = "//img.rocket4app.com/images/logo_black.jpg";
$og_description = "Postpay for your mobile game and app promotion services by hiring professionals with extensive experience. Pay only for results! | Rocket4App";
$og_url = $canonical;

$page_data = array (
		"title" => "Promoting Android and iOS mobile apps on Google Play and the App Store, boosting mobile apps into the top rankings",
		// "description" => "Закажите продвижение мобильных приложений Android Андроид в Google Play и IOS в AppStore от профессионалов. Раскрутку мобильных приложений и вывод в топ Вы можете заказать уже сейчас на нашем сайте",
		"description" => "Postpay for your mobile game and app promotion services by hiring professionals with extensive experience. Pay only for results! | Rocket4App",
		"h1" => "<h1 class='header_info_title' style='width: 400px;'>Boosting mobile apps on the Google Play and App Store top charts</h1>",
		// "h1" => "<h1 class='header_info_title' style='width: 390px;'>Продвижение мобильного приложения <small>в Топ Google Play и AppStore</small></h1>" //"Продвижение Вашего мобильного приложения в топ Google Play и AppStore"
		"h2" => array (
				"0" => "<h2>Продвижение мобильного приложения наш конек!<h2>",
				"1" => "Submit a request<br>for a quote<br><small style='padding-top:10px'>for app or game promotion services</small>",
				"2" => "Написать <strong>хорошее приложение</strong> – лишь половина успеха.<br> Вторая	половина – <strong>успешное продвижение приложения!</strong>",
				"3" => "Почему стоит<br> продвигать приложение с нами:" 
		) 
);

$yellow_title = "Оrder your app promotion services right now";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">

		<?php //<meta http-equiv="X-UA-Compatible" content="IE=edge chrome=1"> ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- SEO Tags -->
<title><?php echo $page_data["title"]; //Главная | Rocket4App ?></title>
<meta name="description"
	content="<?php echo $page_data["description"]; ?>">
<meta name="keywords"
	content="Promotion mobile app, promotion mobile games" />
<meta name="mailru-verification" content="4d1ddbccaa6f9862" />
<!-- /SEO Tags -->
<!-- OG Tags -->
<meta http-equiv="content-language" content="ru">
<meta property="og:site_name" content="<?php echo $og_site_name; ?>" />
<meta property="og:title" content="<?php echo $og_title; ?>" />
<meta property="og:image" content="<?php echo $og_image; ?>" />
<meta property="og:description" content="<?php echo $og_description; ?>" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<meta property="og:type" content="website" />
<!-- /OG Tags -->

<link rel="shortcut icon" href="/favicon.ico">
		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="ru"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>
		
		<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="js/main.js"></script>

<style>
.why_list a, .und {
	text-decoration: underline;
}
</style>
</head>
<body class="homepage">

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<section>
			<div class="header">
				<div class="container">

					<!-- Nav -->
						<?php include_once('navigator-top.php'); ?>
						<!-- /Nav -->

					<link rel="stylesheet" type="text/css" href="/css/style.css">

					<!-- Header Info -->
					<div class="header_info">
						<div class="container">

							<div class="header_info_left">
									<?php
									// <h1 class="header_info_title">
									// Поднимем Ваше<br> мобильное приложение<br> <small>в Топ AppStore<br>
									// и Google Play
									// </small>
									// </h1>
									echo $page_data ["h1"];
									?>
									
									<a href="#about-top" class="btn btn_white"><span
									class="icon-more"></span>Find out how</a>

							</div>

							<div class="header_info_right">

								<div class="header_info_form">

									<h2 class="header_info_form_title">
											<?php
											// Оставить заявку <small>на продвижение</small>
											echo $page_data ["h2"] ["1"];
											?>
										</h2>

									<form method="POST" action="mail.php"
										onsubmit="yaCounter31274678.reachGoal('FORM'); return true;">

										<div class="header_info_form_controls">
											<div class="form-group has-icon">
												<input type="text" name="name" class="form-control __no-bg"
													placeholder="Your name"> <span
													class="form-control-icon icon-user-white"></span>
											</div>
											<div class="form-group has-icon">
												<input type="text" name="phone" class="form-control __no-bg"
													placeholder="Your phone"> <span
													class="form-control-icon icon-phone-white"></span>
											</div>
											<div class="form-group has-icon">
												<input type="text" name="email" class="form-control __no-bg"
													placeholder="or your e-mail"> <span
													class="form-control-icon icon-envelope-white" required></span>
											</div>
										</div>

										<div class="form-group form-group-button">
											<button type="submit" name="submit" class="btn btn_apply">
												<span class="icon-apply"></span>Submit a request
											</button>
										</div>

									</form>

								</div>

							</div>

						</div>
					</div>
					<!-- -->

				</div>
			</div>
		</section>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Seo -->
			<section>
				<div class="seo" style="height: 560px">
					<div class="container">
						<div class="in" style="padding-top: 20px">

							<div class="seo_arrow">
								<span class="icon-arrow-down"></span>
							</div>

							<a id="about-top"></a>
							<h1 class="seo_title" style="width: 600px; margin-left: 180px;">
								+++ Promote your mobile app on the Google Play and App Store top charts 
								for just $200. The process is quick and entails no risk on your side
							</h1>

							<div class="divider"></div>

							<div class="seo_text" style="text-align: left; font-size: 14px">
								<p>
									<strong>Do you want to <a class="und" href="/googleplay/">see
											your app in the top charts </a> and <a class="und"
										href="/installs/" title="">get a great deal of additional installs</a>?
									</strong> <br /> <br /> The answer is probably yes. After all,
									when you launch your app, you will join the market with
									 1.5 million other mobile apps. <br /> 
									 Do you find it
									pointless to try to complete with brands and developers that
									spend hundreds of thousands of dollars on advertising? You might
									think so. And now there is a way to do it and succeed! <br /> <br />
									We can help you get your apps and games on the top charts
									WITHOUT ANY INVESTMENT! <br /> <br /> <strong>We offer working
										conditions </strong>, that will:
								
								
								<ul>
									<li>do not require any initial investment</li>
									<li>make your app more views</li>
									<li>increase the number of downloads of your app</li>
									<li>enhance audience loyalty</li>
								</ul>

								</p>
							</div>

						</div>
					</div>
				</div>
			</section>
			<!-- /Seo -->

			<!-- How it works -->
			<section>
				<div class="how-it-works">
					<div class="container">

						<h2 class="how-it-works_title section_title">
							In other words, we will increase your income!<br /> <small>You
								only spend $200!</small>
						</h2>

						<div class="how-it-works_scheme">
							<img src="//img.rocket4app.com/images/scheme_en.png"
								alt="How it works!">
						</div>

						<!--div class="how-it-works_text">
								<h1 style="font-weight: 600;font-size: 110%;">Продвижение мобильных приложений Андроид в Google Play и IOS в AppStore с компанией Rocket4App</h1>
								<p>
								Наши технологии производят необходимое количество установок за
								кратчайшие сроки<br> для реактивного продвижения приложений в
								Топ.<br> Главная задача - получение наибольшего количества
								органических установок.
								</p>
							</div-->

					</div>
				</div>
			</section>
			<!-- How it works -->

			<!-- Advantages -->
			<div class="advantages inner_shadow">
				<div class="container">
						
						<?php include_once('advantages_promotion.php'); ?>
						
					</div>
			</div>
			<!-- Advantages -->

			<!-- Why -->
			<section>
				<div class="why inner_shadow">
					<div class="container"
						style="padding-left: 0px; padding-bottom: 0px; padding-top: 30px;">

						<a id="about-us"></a>
						<h2 class="why_title"
							style="text-align: right; width: 850px; margin-left: 50px ! important;">
							Rocket4App - guaranteed promotion campaigns for your apps at a
							reasonable price</h2>

							<ol class="why_list"
								style="padding-left: 250px; background: transparent url(//img.rocket4app.ru/images/numbers1.png) no-repeat scroll 270px 40%;">
								<li>We collect our fee from your revenue. So, <a
									href="/prices.php"
									title="You will be surprised at how low our prices are!">prices for
										promotions</a> are minimal and much lower than display ads,
									banners, and paid reviews. That's why, you will get a result with
									minimal financial costs.
								</li>
								<li>We are confident in our abilities, and we know <a
									href="/googleplay/">all the subtleties</a> of mobile app
									promotion. So, we are not afraid of working on a
									postpaid basis. So rest assured: you will not lose your money
									under any circumstances.
								</li>
								<li>We know how to promote your app. All you have to do is sit
									back and watch your audience growth, while saving your time
									and energy.</li>
								<li>We have 5 years of experience working with the app promotion
									market. In the past we worked individually, but now we are 
									working as a team. <a href="#send_me">If you choose to work 
									with us</a>, you can get valuable organic traffic in a short 
									time, which means you will get your revenue.
								</li>
								<li>We are always fine to answer your questions. Our team
									consists of just six people. We focus all of our attention on
									your project. There is no bureaucracy. Therefore, we will always 
									stay your friend and advisor.</li>
								<li>Our results speak for themselves. You can check them 
									yourself if you check<a href="/cases.php">our cases</a>page out. We
									have laid everything out in a transparent, clear, and
									understandable way. Advertising costs will pay off fast, and
									your app will start to make a profit. Of course, in order to get this
									far you have to know what you are doing. <b>We — do!</b>
								</li>
							</ol>

							<div class="why_rocket" style="bottom: 0px;"></div>
					
					</div>
				</div>
			</section>
			<!-- /Why -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">
						
						<?php include_once('sendform-yellow.php'); ?>
						
					</div>
			</div>
			<!-- /Apply -->

			<!-- Reviews -->
			<div class="reviews inner_shadow">
				<div class="container">
						
						<?php include_once('reviews.php'); ?>
						
					</div>
			</div>
			<!-- /Reviews -->

			<!-- Clients -->
			<div class="clients">
				<div class="container">
						
						<?php include_once('workwithus.php'); ?>
						
					</div>
			</div>
			<!-- /Clients -->

			<!-- Map -->
			<?php include_once('map.php'); ?>
			<!-- /Map -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<footer>
		<!-- Footer -->
		<div class="footer-wrapper">
			<div class="footer">
				<div class="container">

					<!-- Nav -->
						<?php include_once('navigator-bottom.php'); ?>
						<!-- /Nav -->

				</div>
			</div>
		</div>
		<!-- /Footer -->

		<!-- Callback Popup -->
			<?php include_once('callbackwnd.php'); ?>
			<!-- /Callback Popup -->
	</footer>

		<script type="application/ld+json">
			{
				"@context": "http://schema.org", 
				"@type": "BreadcrumbList", 
				"itemListElement": [{
					"@type": "ListItem", 
					"position": 1, 
					"item": {
						"@id": "https://rocket4app.com", 
						"name": "Rocket4App",
						"image": "https://img.rocket4app.com/images/maintenance_rocket.png"
					}
					
				}]
			}
		</script>

</body>
</html>
