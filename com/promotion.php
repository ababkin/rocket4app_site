<?php
$lastmod_day = 20;
$lastmod_month = 8;
$lastmod_year = 2017;
$lastmod_min = 39;
$lastmod_hour = 2;
include ("en/redirect.php");

$canonical = "//rocket4app.com/promotion.php";
$alternateEn = "//rocket4app.ru/promotion.php";

$page = array (
		"title" => "Order mobile app promotion services to get your software in the Google Play & App Store top rankings with Rocket4App!",
		"description" => "Postpay for your mobile promotion! We offer the lowest prices! We can be your publisher! A small team of professionals with extensive experience will boost your app into the top rankings of any of the mobile app stores - Rocket4App",
		"h1" => "<h1 class='header_info_title' style='width: 390px;'>Are you confused about how to get into the Google Play and App Store top rankings? 
 <br/><small>We can help you!</small></h1>", // "Продвижение Вашего мобильного приложения в топ Google Play и AppStore",
		"h2" => array (
				"0" => "<h2>Продвижение мобильного приложения наш конек!<h2>",
				"1" => "Submit a request <br>for a quote <br><small style='padding-top:10px'>for mobile app or game promotion services</small>",
				"2" => "Writing  <strong>a good app </strong> – is only half the battle.<br> The second part is <strong>successfully promoting your app! </strong>",
				"3" => "Why you should promote your app with us:" 
		) 
);

$yellow_title = "Boost your app into the top rankings right now! ";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">
		<?php //<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- SEO Tags -->
<title><?php echo $page["title"]; //Главная | Rocket4App ?></title>
<meta name="description" content="<?php echo $page["description"]; ?>">
<!-- /SEO Tags -->
<!-- OG Tags -->
<meta http-equiv="content-language" content="ru">
<meta property="og:site_name" content="Rocket4App" />
<meta property="og:title"
	content="Rocket4App - promotion mobile apps and games" />
<meta property="og:image" content="//img.rocket4app.com/images/logo.jpg" />
<meta property="og:description"
	content="Бесплатный вывод приложений в ТОП." />
<meta property="og:url"
	content="//<?php echo $_SERVER ['SERVER_NAME']; ?>/" />
<!-- /OG Tags -->

<link rel="shortcut icon" href="/favicon.ico">
		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="ru"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>
		
		<link rel="stylesheet" type="text/css" href="/css/style.css">

<script type="text/javascript" src="js/jquery-1.9.0.min.js" async></script>
<script type="text/javascript" src="js/jquery.placeholder.min.js" async></script>
<script type="text/javascript" src="js/owl.carousel.js" async></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js" async></script>
<script type="text/javascript" src="js/main.js" async></script>

<style>
.blue_links a {
	color: #0184D7;
	font-weight: 600;
}
;
</style>
</head>
<body class="homepage">

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<section>
			<div class="header">
				<div class="container">

					<!-- Nav -->
					<?php include_once('navigator-top.php'); ?>
					<!-- /Nav -->

					<!-- Header Info -->
					<div class="header_info">
						<div class="container">

							<div class="header_info_left">
								<?php
								// <h1 class="header_info_title">
								// Поднимем Ваше<br> мобильное приложение<br> <small>в Топ AppStore<br>
								// и Google Play
								// </small>
								// </h1>
								echo $page ["h1"];
								?>
								
								<a href="#about-top" class="btn btn_white"><span
									class="icon-more"></span>Узнайте как</a>

							</div>

							<div class="header_info_right">

								<div class="header_info_form">

									<h2 class="header_info_form_title">
										<?php
										// Оставить заявку <small>на продвижение</small>
										echo $page ["h2"] ["1"];
										?>
									</h2>

									<form method="POST" action="mail.php">

										<div class="header_info_form_controls">
											<div class="form-group has-icon">
												<input type="text" name="name" class="form-control __no-bg"
													placeholder="Your name"> <span
													class="form-control-icon icon-user-white"></span>
											</div>
											<div class="form-group has-icon">
												<input type="text" name="phone" class="form-control __no-bg"
													placeholder="Your phone"> <span
													class="form-control-icon icon-phone-white"></span>
											</div>
											<div class="form-group has-icon">
												<input type="text" name="email" class="form-control __no-bg"
													placeholder="Your e-mail"> <span
													class="form-control-icon icon-envelope-white"></span>
											</div>
										</div>

										<div class="form-group form-group-button">
											<button type="submit" name="submit" class="btn btn_apply">
												<span class="icon-apply"></span>Submit a request
											</button>
										</div>

									</form>

								</div>

							</div>

						</div>
					</div>
					<!-- -->

				</div>
			</div>
		</section>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Seo -->
			<section>
				<div class="seo">
					<div class="container">
						<div class="in">

							<div class="seo_arrow">
								<span class="icon-arrow-down"></span>
							</div>

							<a id="about-top"></a>
							<h2 class="seo_title">
								<?php
								// Написать хорошее приложение – лишь половина успеха.<br> Вторая
								// половина – правильно его продвинуть.</strong>
								echo $page ["h2"] ["2"];
								?>
							</h2>

							<div class="divider"></div>

							<div class="seo_text blue_links">
								<p>
									The success of your future project hangs on choosing the
									correct approach to <a href="/promotion/"
										title="Boosting your app with the Rocket4App"> app promotion.</a>Even
									if your project is perfect, no one will know about it without
									promotion. We provide services so that people will not only
									learn about your app, but they will also download it. We have
									one of the lowest costs for paid installs on the market. This
									is a policy that our company adheres to when it goes to promote
									its own products. We do this for a <a href="/prices.php">reasonable
										price</a>, , since we are confident of the quality of the <a
										href="/cases.php">projects</a>that we sell. The only thing
									that you need to do is to entrust your project to our experts,
									and you will get a quality, finished result!
								</p>
							</div>

						</div>
					</div>
				</div>
			</section>
			<!-- /Seo -->

			<!-- How it works -->
			<section>
				<div class="how-it-works">
					<div class="container">

						<h2 class="how-it-works_title section_title">
							Receive up to 250% more organic installs <br> <small>by getting
								your app into the top rankings </small>
						</h2>

						<div class="how-it-works_scheme">
							<img src="//img.rocket4app.com/images/scheme.png"
								alt="How it works">
						</div>

						<div class="how-it-works_text">
							<h1 style="font-weight: 600; font-size: 110%;">Promote your
								Android mobile apps on Google Play and iOS apps on the App Store
								with Rocket4App</h1>
							<p>Our technologies produce the required number of installs in
								the shortest possible time in order to rocket propel your app
								into the top rankings. The main objective is to obtain the
								greatest number of organic installs.</p>
						</div>

					</div>
				</div>
			</section>
			<!-- How it works -->

			<!-- Advantages -->
			<div class="advantages inner_shadow">
				<div class="container">
						
						<?php include_once('advantages.php'); ?>
						
					</div>
			</div>
			<!-- Advantages -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">
						
						<?php include_once('sendform-yellow.php'); ?>
						
					</div>
			</div>
			<!-- /Apply -->

			<!-- Why -->
			<section>
				<div class="why inner_shadow">
					<div class="container">

						<a id="about-us"></a>
						<h2 class="why_title">
							<?php
							// Почему стоит<br> выводить приложение в топ с нами
							echo $page ["h2"] ["3"];
							?>
						</h2>

						<ol class="why_list">
							<li>More than half of all apps that are installed are selected by
								users from the leader charts</li>
							<li>Once an app is listed at the top of the rankings, the app
								will gain a huge influx of additional installs</li>
							<li>Getting into the top rankings is the most effective way to
								promote your app in terms of the cost per install</li>
							<li>Achieving a spot in the top rankings provides an excellent
								opportunity for additional PR</li>
						</ol>

						<div class="why_rocket"></div>

					</div>
				</div>
			</section>
			<!-- /Why -->

			<!-- Reviews -->
			<div class="reviews inner_shadow">
				<div class="container">
						
						<?php include_once('reviews.php'); ?>
						
					</div>
			</div>
			<!-- /Reviews -->

			<!-- Clients -->
			<div class="clients">
				<div class="container">
						
						<?php include_once('workwithus.php'); ?>
						
					</div>
			</div>
			<!-- /Clients -->

			<!-- Map -->
				<?php include_once('map.php'); ?>
				<!-- /Map -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<footer>
		<!-- Footer -->
		<div class="footer-wrapper">
			<div class="footer">
				<div class="container">

					<!-- Nav -->
						<?php include_once('navigator-bottom.php'); ?>
						<!-- /Nav -->

				</div>
			</div>
		</div>
		<!-- /Footer -->

		<!-- Callback Popup -->
			<?php include_once('callbackwnd.php'); ?>
			<!-- /Callback Popup -->
	</footer>

	<script type="application/ld+json">
		{
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [
				{
					"@type": "ListItem",
					"position": 1,
					"item": {
							"@id": "https://rocket4app.com",
							"name": "Rocket4App",
							"image": "https://img.rocket4app.com/images/maintenance_rocket.png"
					}
				},
				{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.com/promotion.php",
						"name": "Promotion",
						"image": "https://img.rocket4app.com/images/advantages.png"
					}
				}
			]
        }
	</script>

</body>
</html>
