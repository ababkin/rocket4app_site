<?php
//include ("../redirect.php");

$canonical = "//rocket4app.com/publisher/";
$alternateEn = "//rocket4app.ru/publisher/";

$page = array (
		"title" => "As part of the Publisher service, we will promote your app in Google Play & the App Store into the top rankings FOR FREE",
		"description" => "The Publisher service allows you to promote your app into the top rankings of Google Play & the App Store without any financial outlay",
		"h1" => "<div class='header_info_title'>Free promotion services<br>for mobile games <br>and apps on <br><small>Google Play &amp; AppStore</small></div>", // "Продвижение Вашего мобильного приложения в топ Google Play и AppStore",
		"h2" => array (
				"0" => "<h2>Продвижение мобильного приложения наш конек!<h2>",
				"1" => "Get your Android or iOS app or game<small> into the top rankings  </small>",
				"2" => "Как раскрутить приложение в Google Play &amp; AppStore?",
				"3" => "Почему важна раскрутка приложения в топ:" 
		) 
);

$yellow_title = "Request a free promotional campaign for getting your app into the top rankings!";
$yellow_btn = "I want to order the free 'Publisher 50/50' promotional service";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- SEO Tags -->
<title><?php echo $page["title"]; //Главная | Rocket4App ?></title>
<meta name="description" content="<?php echo $page["description"]; ?>">
<!-- /SEO Tags -->

<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="shortcut icon" href="/favicon.ico">
		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="ru"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>
		<style>
p {
	padding-top: 10px;
}

blockquote {
	background-color: rgba(0, 255, 0, 0.3);
	padding: 15px;
	margin: 20px 0 0 0;
	font-size: 20px;
	font-weight: 600;
}

h3 {
	padding-top: 40px;
}

ol>li {
	font-size: 20px;
}

h3 a {
	color: #45c3c8;
}
</style>
</head>
<body class="homepage">

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
					<?php include_once('../navigator-top.php'); ?>
					<!-- /Nav -->

				<!-- Header Info -->
				<div class="header_info">
					<div class="container">

						<div class="header_info_left">
								<?php
								// <h1 class="header_info_title">
								// Поднимем Ваше<br> мобильное приложение<br> <small>в Топ AppStore<br>
								// и Google Play
								// </small>
								// </h1>
								echo $page ["h1"];
								?>
								
								<a href="#about-top" class="btn btn_white"><span
								class="icon-more"></span>Find out how</a>

						</div>

						<div class="header_info_right">

							<div class="header_info_form">

								<h2 class="header_info_form_title">
										<?php
										// Оставить заявку <small>на продвижение</small>
										echo $page ["h2"] ["1"];
										?>
									</h2>

								<form method="POST" action="mail.php">

									<div class="header_info_form_controls">
										<div class="form-group has-icon">
											<input type="text" name="name" class="form-control __no-bg"
												placeholder="Your name"> <span
												class="form-control-icon icon-user-white"></span>
										</div>
										<div class="form-group has-icon">
											<input type="text" name="phone" class="form-control __no-bg"
												placeholder="Your phone"> <span
												class="form-control-icon icon-phone-white"></span>
										</div>
										<div class="form-group has-icon">
											<input type="text" name="email" class="form-control __no-bg"
												placeholder="Your e-mail"> <span
												class="form-control-icon icon-envelope-white"></span>
										</div>
									</div>

									<div class="form-group form-group-button">
										<button type="submit" name="submit" class="btn btn_apply">
											<span class="icon-apply"></span>Submit a request
										</button>
									</div>

								</form>

							</div>

						</div>

					</div>
				</div>
				<!-- -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Seo -->
			<div class="seo">
				<div class="container">
					<div class="in">

						<div class="seo_arrow" style="margin-top: 30px;">
							<span class="icon-arrow-down"></span>
						</div>

						<a name="about-top"></a>
						<h1 class="seo_title">
								<?php
								// Написать хорошее приложение – лишь половина успеха.<br> Вторая
								// половина – правильно его продвинуть.</strong>
								echo "Do you want to position your app in the Google Play or App Store top rankings?"; // $page["h2"]["2"];
								?>
							</h1>

						<div class="divider"></div>

						<div class="seo_text" style="text-align: left">
							<p>So you have come up with a wonderful mobile app, and now do you want to climb to the top of the Google Play or App Store rankings, conquer the world, and make money?</p>
							<p>Alas, the process is not quite so simple. Many wonderful programs have been swept into the dustbin of history because their creators did not have enough money to promote their masterpieces. </p>
							<br />
							<h2 class="seo_title">Special offer for mobile app developers</h2>
							<p>A properly developed marketing strategy and financial support will allow you to not only to recoup the costs of creating the app, but also to earn a nice round sum as a profit. </p>
							<p>If you have a worthwhile app that will strike a chord with users, —then you should </p>

						</div>

					</div>
				</div>
			</div>
			<!-- /Seo -->

			<!-- How it works -->
			<div class="how-it-works"
				style="margin-top: 130px; margin-bottom: 30px;">
				<div class="container">

					<h2 class="how-it-works_title section_title"
						style="background: url(//img.rocket4app.com/images/section_title.png) no-repeat 50% 0;">
						take advantage of our "Publisher 50/50" service <br> <small>from
							Rocket4app</small>
					</h2>

					<div class="seo_text" style="text-align: left">

						<p>With our support, you will be able to reach the top rankings of the mobile app stores without having to spend a single penny. The main thing is that you have an interesting idea that you have implemented in an Android or iOS app. </p>
						<br>
						<p>The essence of the service is simple: you develop an interesting product, and we assess its potential and promote the app into the top rankings. We will publish your app in Google Play, register it for free in the App Store, and keep track of the analytics. Generally speaking, we provide a full range of promotional services. </p>
						<br>
						<p>And we will split the profits in half. We offer an honest and mutually beneficial proposal, which you would be hard put to find anywhere else. Because getting your app into the top rankings always entails risk. </p>
						<br>
						<p>However, we have enough experience to feel confident about achieving success in this risky business. Our practical experience, foresight, and intuition allow us to accurately predict the success of a particular app. Examples of our professionalism can be found on our website in the "Case studies" section. </p>

					</div>
				</div>
			</div>
			<!-- /Hot-it-works -->

			<!-- Apply -->
			<div class="apply inner_shadow"
				class="margin-top:-55px;margin-bottom:30px;">
				<div class="container" id="order">
						
						<?php include_once('../sendform-yellow.php'); ?>
						
					</div>
			</div>
			<!-- /Apply -->

			<!-- Hot-it-works -->
			<div class="how-it-works"
				style="padding-top: 30px; margin-bottom: 25px;">
				<div class="container">
					<div class="seo_text" style="text-align: left">
						<h2 class="seo_title">How to make money on your apps</h2>

						<ol>
							<li start=1>Free app.</li>
							<p>You create an app with free functionality, and the monetization is achieved through additional (paid) features and tools. Mobile app analytics show that to date free applications have accounted for 60% of the offerings in the App Store and 80% of those in Google Play. </p>
							<br>
							<li start=2>Paid app.</li>
							<p>This option is not particularly popular, but it is also a justifiable strategy. In order for this scheme to work, your app must be truly unique and super-interesting. </p>
							<br>
							<li start=3>Subscription.</li>
							<p>If you are able to constantly provide new content, add useful services, and charge users a regular fee, then by all means try the subscription model. But this method requires you to invest a lot of time, and therefore it is perhaps the most appropriate for news publishers and content providers. </p>
							<br>
							<li start=4>In-app advertising</li>
							<p>You sell advertising space in your app to brands, whose products will appeal to your target audience. </p>
						</ol>
						<blockquote>If you have a finished product or it is still under development, and you want it to earn money, then go for this option! There is a chance that your app will take off and achieve the top rankings during the first few days after publication. </blockquote>
						<br> <br>
						<p>If you sign up for our Publisher service, we will be able to help you resolve all issues related to promotion. </p>

						<h3 class="seo_title">
							Sounds good?<br> Then <a href="#order">fill out this simple form</a><br> and let's get to work! 
						</h3>
					</div>
				</div>
			</div>
			<!-- How it works -->


			<!-- Clients -->
			<div class="clients">
				<div class="container">
						
						<?php include_once('../workwithus.php'); ?>
						
					</div>
			</div>
			<!-- /Clients -->

			<!-- Map -->
				<?php include_once('../map.php'); ?>
				<!-- /Map -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
					<?php include_once('../navigator-bottom.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="/js/owl.carousel.js"></script>
	<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>

	<!-- Callback Popup -->
		<?php include_once('../callbackwnd.php'); ?>
		<!-- /Callback Popup -->

	<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "BreadcrumbList",
				"itemListElement": [{
					"@type": "ListItem",
					"position": 1,
					"item": {
						"@id": "https://rocket4app.com",
						"name": "Rocket4App",
							"image": "https://img.rocket4app.com/images/maintenance_rocket.png"
					}
					},{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.com/publisher/",
						"name": "Free promotion of games"
					}
				}]
			}
		</script>
</body>
</html>
