<?php
$lastmod_day = 20;
$lastmod_month = 8;
$lastmod_year = 2017;
$lastmod_min = 9;
$lastmod_hour = 2;
include ("en/redirect.php");

$canonical = "//rocket4app.com/partners.php";
$alternateEn = "//rocket4app.ru/partners.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Partnership – promoting your mobile apps | Rocket4App</title>
<meta name="description"
	content="Options for developers and resellers to partner with Rocket4App to get their apps into the top rankings">

<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="shortcut icon" href="/favicon.ico">

		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="ru"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>


</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
				<?php include_once('navigator-top.php'); ?>
				<!-- /Nav -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Page Header -->
			<div class="page-header">
				<div class="container">
					<div class="in">

						<h1 class="page-header_title">Partnership</h1>

						<div class="divider"></div>

					</div>
				</div>
			</div>
			<!-- /Page Header -->

			<!-- Cooperation -->
			<div class="cooperation">
				<div class="container">

					<div class="item post __reseller">

						<div class="item_image">
							<img src="//img.rocket4app.com/images/content/reseller.png"
								alt="Реселлерам">
						</div>

						<h2 class="item_title section_title">
							For<br> <small>Resellers</small>
						</h2>

						<p>
							<strong>
							Earn with us and help others ramp their games and apps
							on Google Play and the App Store up!
							</strong>
						</p>

						<p>Offer our services to your customers and receive a percentage
							of their payments!</p>
						<p>The more customers you find, the more revenue you will receive
							from each transaction.</p>
						<p>Keep in mind that our average transaction is $5,000, and we are
						willing to share up to 25% to good resellers as a referral fee.
						</p>



					</div>

					<div class="dashed_divider"></div>

					<div class="item post __developer">

						<div class="item_image">
							<img src="//img.rocket4app.com/images/content/developer.png"
								alt="For developers mobile apps and games">
						</div>

						<h2 class="item_title section_title">
							For<br> <small>Developers</small>
						</h2>

						<p>
							<strong>Have you made an interesting game or app, but you do not have
							the technology or tools to promote your software? Do you know how to
							boost your app and get on the top charts on the mobile app stores?
							</strong>
						</p>

						<p>
							We offer <a style="color: rgb(0, 161, 236); font-weight: 600;"
								href="/publisher/"
								title="Узнайте все о бесплатном продвижении мобильных приложений и игр">a
								unique Publisher service</a>
						</p>
						<p>
							Publish with us, advertise your games for free, and get on the
							<a href="/googleplay/"
								title="Закажите вывод в топ вашего мобильного приложения">Google
								Play and the App Store top charts!</a>
						</p>
						<p>We will help you with getting your app on the top charts on the app
							stores of the most popular countries for a share of the future
							revenue.</p>
						<p>We practice an individualized approach for each project.</p>

					</div>

				</div>
			</div>
			<!-- /Cooperation -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">

						<?php include_once('sendform-yellow.php'); ?>

					</div>
			</div>
			<!-- /Apply -->

			<!-- Clients -->
			<div class="clients inner_shadow">
				<div class="container">

					<?php include_once('workwithus.php'); ?>

				</div>
			</div>
			<!-- /Clients -->

			<!-- Map -->
			<?php include_once('map.php'); ?>
			<!-- /Map -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
                <?php include_once('navigator-bottom.php'); ?>
                <!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="/js/owl.carousel.js"></script>
	<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>

	<!-- Callback Popup -->
	<?php include_once('callbackwnd.php'); ?>
	<!-- /Callback Popup -->

	<script type="application/ld+json">
		{
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [
				{
					"@type": "ListItem",
					"position": 1,
					"item": {
							"@id": "https://rocket4app.com",
							"name": "Rocket4App",
							"image": "https://img.rocket4app.com/images/maintenance_rocket.png"
					}
				},
				{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.com/partners.php",
						"name": "Cooperation",
						"image": "https://img.rocket4app.com/images/content/developer.png"
					}
				}
			]
        }
	</script>
</body>
</html>
