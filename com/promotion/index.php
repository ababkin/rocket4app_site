<?php
$lastmod_day = 20;
$lastmod_month = 8;
$lastmod_year = 2017;
$lastmod_min = 32;
$lastmod_hour = 16;
include ("../redirect.php");

$canonical = "//rocket4app.com/promotion/";
$alternateEn = "//rocket4app.ru/promotion/";

$page = array (
		"title" => "Order mobile game and app promotion services for Google Play and the App Store",
		"description" => "Postpay to promote your mobile apps and games on Google Play and the App Store",
		"h1" => "<h1 class='header_info_title'>Mobile app promotion<br> services for <br> <small>Google Play<br>and the AppStore <br></small></h1>", // "Продвижение Вашего мобильного приложения в топ Google Play и AppStore",
		"h2" => array (
			"0" => "<h2>Our company provides you with the advantage you need to achieve success!<h2>",
			"1" => "Order promotion services <small>for your Android and iOS apps </small>",
			"2" => "How do you promote your app in Google Play & the App Store? ",
			"3" => "Why it is important to get your app into the top rankings:"
		) 
);

$yellow_title = "Boost your app with us. No prepayment required!";
$yellow_btn = "Send a request for a quote to promote your app into the top rankings";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<!-- SEO Tags -->
<title><?php echo $page["title"]; //Главная | Rocket4App ?></title>
<meta name="description" content="<?php echo $page["description"]; ?>">
<!-- /SEO Tags -->

<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="shortcut icon" href="/favicon.ico">
		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="ru"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>
		
	</head>
<body class="homepage">

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
					<?php include_once('../navigator-top.php'); ?>
					<!-- /Nav -->

				<!-- Header Info -->
				<div class="header_info">
					<div class="container">

						<div class="header_info_left">
								<?php
								// <h1 class="header_info_title">
								// Поднимем Ваше<br> мобильное приложение<br> <small>в Топ AppStore<br>
								// и Google Play
								// </small>
								// </h1>
								echo $page ["h1"];
								?>
								
								<a href="#about-top" class="btn btn_white"><span
								class="icon-more"></span>Find out how</a>

						</div>

						<div class="header_info_right">

							<div class="header_info_form">

								<h2 class="header_info_form_title">
										<?php
										// Оставить заявку <small>на продвижение</small>
										echo $page ["h2"] ["1"];
										?>
									</h2>

								<form method="POST" action="mail.php">

									<div class="header_info_form_controls">
										<div class="form-group has-icon">
											<input type="text" name="name" class="form-control __no-bg"
												placeholder="Your name"> <span
												class="form-control-icon icon-user-white"></span>
										</div>
										<div class="form-group has-icon">
											<input type="text" name="phone" class="form-control __no-bg"
												placeholder="Your phone"> <span
												class="form-control-icon icon-phone-white"></span>
										</div>
										<div class="form-group has-icon">
											<input type="text" name="email" class="form-control __no-bg"
												placeholder="Your e-mail"> <span
												class="form-control-icon icon-envelope-white"></span>
										</div>
									</div>

									<div class="form-group form-group-button">
										<button type="submit" name="submit" class="btn btn_apply">
											<span class="icon-apply"></span>Submit a request
										</button>
									</div>

								</form>

							</div>

						</div>

					</div>
				</div>
				<!-- -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Seo -->
			<div class="seo">
				<div class="container">
					<div class="in">

						<div class="seo_arrow">
							<span class="icon-arrow-down"></span>
						</div>

						<a name="about-top"></a>
						<h2 class="seo_title">
								<?php
								// Написать хорошее приложение – лишь половина успеха.<br> Вторая
								// половина – правильно его продвинуть.</strong>
								echo $page ["h2"] ["2"];
								?>
							</h2>

						<div class="divider"></div>

							<div class="seo_text">
								<p>
								Currently the market is flooded with a huge number of mobile apps and games. 
								You may think that your app will be unique and that everyone will immediately 
								fall in love with it, but its popularity and reach depend on the platforms 
								across which it is distributed. When you publish your app or mobile game, 
								it is just the beginning of a long journey.  
								</p>
								<br/>
								<p>
								<b>Your mobile app will only become popular and profitable by getting into 
								the top rankings!</b>
								</p>
								<br>
								
								<div style="text-align:left">
								The following criteria influence your ability to climb the rankings: 
									<ul>
										<li>Number of app installs</li>
										<li>Positive reviews</li>
										<li>Ratings</li>
									</ul>
								</div>
								
								<p>We can provide all of these services on a short turnaround and at the 
								lowest price on the market without the need for prepayment! For a small 
								fee we will give a boost to your app so that it will reach the top rankings, 
								where it will receive a new impetus in the form of organic installs! The 
								only thing that you need to do is to entrust your project to our experts, 
								and you will get a quality, finished result!
								</p>
							</div>

					</div>
				</div>
			</div>
			<!-- /Seo -->

			<!-- How it works -->
			<div class="how-it-works">
				<div class="container">

					<h2 class="how-it-works_title section_title">
						Receive up to 250% more organic installs<br> 
						<small>by getting your app into the top rankings</small>
					</h2>

					<div class="how-it-works_scheme">
						<img src="//img.rocket4app.com/images/scheme_en.png"
							alt="How it work">
					</div>

					<div class="how-it-works_text">
						<p>
							Our technologies produce the required number of installs 
							in the shortest possible time in order to rocket propel 
							your app into the top rankings. The main objective is to 
							obtain the greatest number of organic installs.
						</p>
					</div>

				</div>
			</div>
			<!-- How it works -->

			<!-- Advantages -->
			<div class="advantages inner_shadow">
				<div class="container">
						
						<?php include_once('../advantages.php'); ?>
						
					</div>
			</div>
			<!-- Advantages -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">
						
						<?php include_once('../sendform-yellow.php'); ?>
						
					</div>
			</div>
			<!-- /Apply -->

			<!-- Why -->
			<div class="why inner_shadow">
				<div class="container">

					<a name="about-us"></a>
					<h2 class="why_title">
							<?php
							// Почему стоит<br> выводить приложение в топ с нами
							echo $page ["h2"] ["3"];
							?>
						</h2>

					<ol class="why_list">
							<li>More than half of all apps that are installed are selected by users from the leader charts</li>
							<li>Once an app is listed at the top of the rankings, the app will gain a huge influx of additional installs</li>
							<li>Getting into the top rankings is the most effective way to promote your app in terms of the cost per install</li>
							<li>Achieving a spot in the top rankings provides an excellent opportunity for additional PR</li>
					</ol>

					<div class="why_rocket"></div>

				</div>
			</div>
			<!-- /Why -->

			<!-- Reviews -->
			<div class="reviews inner_shadow">
				<div class="container">
						
						<?php include_once('../reviews.php'); ?>
						
					</div>
			</div>
			<!-- /Reviews -->

			<!-- Clients -->
			<div class="clients">
				<div class="container">
						
						<?php include_once('../workwithus.php'); ?>
						
					</div>
			</div>
			<!-- /Clients -->

			<!-- Map -->
				<?php include_once('../map.php'); ?>
				<!-- /Map -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
					<?php include_once('../navigator-bottom.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="/js/owl.carousel.js"></script>
	<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>

	<!-- Callback Popup -->
		<?php include_once('../callbackwnd.php'); ?>
		<!-- /Callback Popup -->

	<script type="application/ld+json">
			{
				"@context": "http://schema.org",
				"@type": "BreadcrumbList",
				"itemListElement": [{
					"@type": "ListItem",
					"position": 1,
					"item": {
						"@id": "https://rocket4app.com",
						"name": "Rocket4App",
							"image": "https://img.rocket4app.com/images/maintenance_rocket.png"
					}
					},{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.com/promotion/",
						"name": "Promotion of mobile applications"
					}
				}]
			}
		</script>
</body>
</html>