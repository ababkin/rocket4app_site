<?php
$cases_list = array (
		array (
				"name" => "House of fear - Prison",
				"brand" => "//img.rocket4app.com/images/cases/Lucy-escape.png" 
		),
		array (
				"name" => "Farm",
				"brand" => "//img.rocket4app.com/images/cases/Farm.png" 
		),
		array (
				"name" => "House 23",
				"brand" => "//img.rocket4app.com/images/cases/House23Escape.png" 
		),
		array (
				"name" => "Poker",
				"brand" => "//img.rocket4app.com/images/cases/NewPoker.png" 
		),
		array (
				"name" => "Escape - fear house",
				"brand" => "//img.rocket4app.com/images/cases/EscapeFearHouse.png" 
		),
		array (
				"name" => "Dog Patrol Paw Ninja",
				"brand" => "//img.rocket4app.com/images/cases/AdventureDogPatrolPawNinja.png" 
		),
		array (
				"name" => "Was wäre wenn?",
				"brand" => "//img.rocket4app.com/images/cases/WasWareWen.png" 
		),
		array (
				"name" => "Try to escape 2",
				"brand" => "//img.rocket4app.com/images/cases/TryToEscape2.png" 
		),
		array (
				"name" => "Stickman jailbreak escape",
				"brand" => "//img.rocket4app.com/images/cases/StickmanJailbreak.png" 
		),
		array (
				"name" => "Rescue Lucy",
				"brand" => "//img.rocket4app.com/images/cases/Lucy.png" 
		) 
);

shuffle ( $cases_list );
?>

<h2 class="clients_title">These companies have already used our services</h2>

<div class="clients_cases text-center">
	<a href="/cases.php" class="clients_cases_link">Our cases...</a>
</div>

<a href="/cases.php"	style="text-decoration: none;">
	<ul class="clients_list">
	    <?php 		for ($i = 0; $i < 5; ++$i) {   ?>
		  <li><span class="item_image"><img
				src="<?php echo $cases_list[$i]["brand"]; ?>"
				alt="Our case: <?php echo $cases_list[$i]["name"];?>"></span> <span
			class="item_title"><?php echo $cases_list[$i]["name"]; ?></span></li>
		<?php } ?>
	</ul>
</a>