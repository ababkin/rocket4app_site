<?php
$lastmod_day = 20;
$lastmod_month = 8;
$lastmod_year = 2017;
$lastmod_min = 28;
$lastmod_hour = 3;
include ("en/redirect.php");

$canonical = "//rocket4app.com/prices.php";
$alternateEn = "//rocket4app.ru/prices.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=1000">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>Price of promotion app | Rocket4App</title>
<meta name="description"
	content="The lowest prices for the promotion of mobile applications, as well as free promotion in the service 'Publisher'">

		<?php if (isset($canonical)): ?><link rel="canonical"
	href="<?php echo $canonical; ?>" /><?php endif; ?>
		<?php if (isset($alternateEn)): ?><link rel="alternate" hreflang="ru"
	href="<?php echo $alternateEn; ?>" /><?php endif; ?>


		<link rel="shortcut icon" href="/favicon.ico">

<style>
a[href="#send_me"] {
	text-decoration: underline;
}
</style>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<!-- Wrapper -->
	<div class="wrapper">

		<!-- Header -->
		<div class="header">
			<div class="container">

				<!-- Nav -->
					<?php include_once('navigator-top.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
		<!-- /Header -->

		<!-- Content -->
		<div class="content">

			<!-- Page Header -->
			<div class="page-header">
				<div class="container">
					<div class="in">

						<h1 class="page-header_title">Price</h1>
						<!-- h3 class="page-header_title" style="font-size: 25px;">В цену
								входит трехдневное удержание в топе</h3>
								<h3 class="page-header_title" style="font-size: 25px;">Цены на
								вывод в других странах узнавайте у наших специалистов</h3>
								<h2 class="page-header_title" style="font-size: 25px;"><strong>Мы не берем
							предоплату</strong> - только по факту!</h2 -->
						<div class="divider"></div>

					</div>
				</div>
			</div>
			<!-- /Page Header -->

			<div class="price-advantage">
				<div class="container">
					<div class="price-adv">
						<div class="adv">
							<span class="adv1"></span>
							<h2>The price includes keeping the app on top for three days</h2>
						</div>
						<div class="adv">
							<span class="adv2"></span>
							<h2>Ask our assistants about the price for other countries</h2>
						</div>
						<div class="adv">
							<span class="adv3"></span>
							<h2>We know our business and we provide 100% success guarantee!</h2>
						</div>

					</div>
				</div>
			</div>

			<!-- Pricing -->
			<div class="pricing">
				<div class="container">

					<h2 class="pricing_title __googleplay">Rank in Google Play Top Lists</h2>

					<table class="pricing_table">
						<thead>
							<tr>
								<th>Country</th>
								<th>1-1000<br>installs
								</th>
								<th>1000-5000<br>installs
								</th>
								<th> > 5000<br>installs
								</th>
								<th>Top 10<br>of category
								</th>
								<th>Top 10<br> of games
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/russia.png"
										alt="The price of promotion to the top of Russia for mobile applications Google Play"></span>
									Russia</td>
								<td class="cell_top1">5.00 RUR</td>
								<td class="cell_top2">4.50 RUR</td>
								<td class="cell_top1">4.00 RUR</td>
								<!-- <td class="cell_top1">
										<div style="color: red;text-decoration: line-through;">1200&nbsp;$</div>
										<div style="font-size: 158%;color: #0DBF0D;">800&nbsp;$</div>
										<div style="position: relative;top: -90px;z-index: 100;left: -63px;height: 0px;">
											<img src="//img.rocket4app.com/images/action-new.png">
										</div>
									</td>-->
								<td class="cell_top2">1000-2000&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="send your request and we will contact you">Ask</a></td>
							</tr>
							<tr class="lightblue">
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/nl.png"
										alt="The price of promotion to the top of the Netherlands for mobile apps Google Play"></span>
									Netherlands</td>
								<td class="cell_top1">0.18&nbsp;$</td>
								<td class="cell_top2">0.16&nbsp;$</td>
								<td class="cell_top1">0.14&nbsp;$</td>
								<td class="cell_top2">200-400&nbsp;$</td>
								<td class="cell_top3">3000&nbsp;$</td>
							</tr>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/uk.png"
										alt="The price of promotion to the top of the UK for mobile apps Google Play"></span>
									UK</td>
								<td class="cell_top1">0.18&nbsp;$</td>
								<td class="cell_top2">0.16&nbsp;$</td>
								<td class="cell_top1">0.14&nbsp;$</td>
								<td class="cell_top2">900-1800&nbsp;$</td>
								<td class="cell_top3">8 000&nbsp;$</td>
							</tr>
							<tr class="lightblue">
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/canada.png"
										alt="The price of output to the top of Canada for mobile apps Google Play"></span>
									Canada</td>
								<td class="cell_top1">0.18&nbsp;$</td>
								<td class="cell_top2">0.16&nbsp;$</td>
								<td class="cell_top1">0.14&nbsp;$</td>
								<td class="cell_top2">500-1000&nbsp;$</td>
								<td class="cell_top3">5 000&nbsp;$</td>
							</tr>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/germany.png"
										alt="The price of output to the top of Germany for mobile applications Google Play"></span>
									Germany</td>
								<td class="cell_top1">0.18&nbsp;$</td>
								<td class="cell_top2">0.16&nbsp;$</td>
								<td class="cell_top1">0.14&nbsp;$</td>
								<td class="cell_top2">1250-2500&nbsp;$</td>
								<td class="cell_top3">10 000&nbsp;$</td>
							</tr>
							<tr class="lightblue">
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/usa.png"
										alt="The price of output to the top US for mobile apps Google Play"></span>
									USA</td>
								<td class="cell_top1">0.16&nbsp;$</td>
								<td class="cell_top2">0.14&nbsp;$</td>
								<td class="cell_top1">0.12&nbsp;$</td>
								<td class="cell_top2">5000-10000&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="send your request and we will contact you">Ask</a></td>
							</tr>
						</tbody>
					</table>


					<h2 class="pricing_title __appstore">Rank in App Store Top Lists</h2>

					<table class="pricing_table">
						<thead>
							<tr>
								<th>Country</th>
								<th>For 1 usual<br> install
								</th>
								<th>For 1 search<br> install
								</th>
								<th>TOP Trending<br> requests
								</th>
							</tr>
						</thead>
						<tbody>
							<tr class="lightviolet">
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/russia.png"
										alt="The price of output to the top of Russia for mobile applications AppStore"></span>
									Russia</td>
								<td class="cell_top1">15 RUR</td>
								<td class="cell_top2">17 RUR</td>
								<td class="cell_top3">45 000 RUR</td>
							</tr>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/uk.png"
										alt="The price of output in the UK top for mobile applications AppStore"></span>
									UK</td>
								<td class="cell_top1">0.70&nbsp;$</td>
								<td class="cell_top2">0.80&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="send your request and we will contact you">Ask</a></td>
							</tr>
							<tr class="lightviolet">
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/canada.png"
										alt="The price of promotion in the top Canada for mobile applications AppStore"></span>
									Canada</td>
								<td class="cell_top1">0.70&nbsp;$</td>
								<td class="cell_top2">0.80&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="send your request and we will contact you">Ask</a></td>
							</tr>
							<tr>
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/germany.png"
										alt="The price of promotion to the top of Germany for mobile applications AppStore"></span>
									Germany</td>
								<td class="cell_top1">0.70&nbsp;$</td>
								<td class="cell_top2">0.80&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="send your request and we will contact you">Ask</a></td>
							</tr>
							<tr class="lightviolet">
								<td class="cell_title"><span><img
										src="//img.rocket4app.com/images/flags/usa.png"
										alt="The price of promotion to the US top for mobile applications AppStore"></span>
									USA</td>
								<td class="cell_top1">0.70&nbsp;$</td>
								<td class="cell_top2">0.80&nbsp;$</td>
								<td class="cell_top3"><a href="#send_me"
									title="send your request and we will contact you">Ask</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /Pricing -->

			<!-- Offers -->
			<div class="offers">
				<div class="container">

					<h2 class="offers_title">We have profitable offers</h2>

					<div class="offers_list">

						<div class="item post __reseller">

							<div class="item_image">
								<a href="/partners.php"><img
									src="//img.rocket4app.com/images/content/reseller.png"
									alt="Services for promotion of applications for Resellers"></a>
							</div>

							<a href="/partners.php" class="item_title"> For resellers </a>

						</div>

						<div class="item post __developer">

							<div class="item_image">
								<a href="/partners.php"><img
									src="//img.rocket4app.com/images/content/developer.png"
									alt="Services for promotion of applications for Developers"></a>
							</div>

							<a href="/partners.php" class="item_title"> For developers </a>

						</div>

					</div>

				</div>
			</div>
			<!-- /Cooperation -->

			<!-- Apply -->
			<div class="apply inner_shadow">
				<div class="container">

						<?php include_once('sendform-yellow.php'); ?>

					</div>
			</div>
			<!-- /Apply -->

		</div>
		<!-- /Content -->

	</div>
	<!-- /Wrapper -->

	<!-- Footer -->
	<div class="footer-wrapper">
		<div class="footer">
			<div class="container">

				<!-- Nav -->
					<?php include_once('navigator-bottom.php'); ?>
					<!-- /Nav -->

			</div>
		</div>
	</div>
	<!-- /Footer -->

	<!-- Callback Popup -->
		<?php include_once('callbackwnd.php'); ?>
		<!-- /Callback Popup -->

	<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="js/owl.carousel.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<script type="application/ld+json">
		{
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [
				{
					"@type": "ListItem",
					"position": 1,
					"item": {
							"@id": "https://rocket4app.com",
							"name": "Rocket4App",
							"image": "https://img.rocket4app.com/images/maintenance_rocket.png"
					}
				},
				{
					"@type": "ListItem",
					"position": 2,
					"item": {
						"@id": "https://rocket4app.com/prices.php",
						"name": "Our prices for promotion",
						"image": "https://img.rocket4app.com/images/content/reseller.png"
					}
				}
			]
        }
	</script>
</body>
</html>
