<div class="popup popup_callback" id="popup-callback">
	<div class="popup_container">

		<div class="popup_header">
			<div class="popup_header_title">Call me</div>
		</div>

		<div class="popup_content">

			<form class="popup_callback_form" method="POST"
				action="//<?php echo $_SERVER['SERVER_NAME']; ?>/mail.php">

				<div class="form-group has-icon">
					<input type="text" name="name" class="form-control __no-bg"
						placeholder="Your name"> <span
						class="form-control-icon icon-user-white"></span>
				</div>

				<div class="form-group has-icon">
					<input type="text" name="phone" class="form-control __no-bg"
						placeholder="Your phone"> <span
						class="form-control-icon icon-phone-white"></span>
				</div>

				<div class="form-group has-icon">
					<input type="text" name="email" class="form-control __no-bg"
						placeholder="Your e-mail" required> <span
						class="form-control-icon icon-phone-white"></span>
				</div>

				<input type="hidden" name="callback" value="callback" />

				<div class="form-group form-group-button">
					<button type="submit" name="submit" class="btn btn_apply">
						<span class="icon-phone-big"></span>Call me
					</button>
				</div>

			</form>

		</div>

		<a href="#" class="popup_close"><i class="icon-close"></i></a>

	</div>
</div>
<script type="text/javascript" src="//consultsystems.ru/script/27499/"
	charset="utf-8" async></script>