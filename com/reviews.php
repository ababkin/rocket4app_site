<h2 class="reviews_title">Our customer feedback</h2>

<div class="reviews_carousel_wrapper">
	<div class="reviews_carousel">

		<div class="item">
			<div class="item_text">
				<cite>It is easy and fun to work with Rocket4App The time between
					when I had my first Skype conversation and when I achieved the
					desired result of a top ranking was just one week. All questions
					and comments were answered by the company's staff in real time.
					They provided excellent customer service.</cite>
			</div>

			<div class="item_author">
				<div class="in">
					<div class="in_left">
						<img src="//img.rocket4app.com/images/reviews/author-1.png"
							alt="Отзыв Андрея из Москвы">
					</div>
					<div class="in_right">
						<strong>Andrey</strong>Moscow
					</div>
				</div>
			</div>
		</div>

		<div class="item">
			<div class="item_text">
				<div class="item_text">
					<cite>This is not the first time that we have used mobile traffic
						aggregator services, but Rocket4App offers the lowest price on the
						market for mobile app promotion services. Despite the discount,
						you still get a quality service with a fast turnaround. These guys
						deserve respect!</cite>
				</div>

			</div>
			<div class="item_author">
				<div class="in">
					<div class="in_left">
						<img src="//img.rocket4app.com/images/reviews/author-2.png"
							alt="Отзыв Людмилы из г.Санкт-Петербург">
					</div>
					<div class="in_right">
						<strong>Lyudmila</strong>St.Peterburg
					</div>
				</div>
			</div>
		</div>

		<div class="item">
			<div class="item_text">
				<cite>Personally, I decided to cooperate with Rocket4App when I
					found out that I would only pay for promoting my game after a
					result was achieved. The service is very cool and very convenient.
					There is no need to wait in nervous anticipation until you find out
					if the campaign worked or not. The contractor performing the
					service assumes all risks of success. I think this a big step
					forward for the Russian mobile market. </cite>
			</div>
			<div class="item_author">
				<div class="in">
					<div class="in_left">
						<img src="//img.rocket4app.com/images/reviews/author-3.png"
							alt="Отзыв Романа из Новосибирска">
					</div>
					<div class="in_right">
						<strong>Roman</strong>Novosibirsk
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
