<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=1000">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<link rel="stylesheet" type="text/css" href="/css/style.css">
		<link rel="stylesheet" type="text/css" href="/css/case.css">
		<?php
			$name = "Escape - fear house";
			$common_target = "Top-10 puzzle games in US";
			$title = "Продвижение приложения в ". $common_target ." в Google Play - Кейс компании Rocket4App | ". $name;
			$description = "Кейс компании: ". $name ." - история вывода приложения в ". $common_target;
			$h1 = $name;
			$targets = array(
						$common_target . ""
					);
			$tasks = array(
						"Top-10 puzzle games in US"
					);
			$flags = array("offers_usa");

			$brand_src = "//img.rocket4app.com/cases/title/escape-fear-house-case.png";
			$shot = "//img.rocket4app.com/cases/title/escape-fear-house-shot.png";
			$diagram = "//img.rocket4app.com/cases/title/escape-fear-house-diagram.png";
			$time = "01.09.2015 - 07.09.2015";
			$notes = "<b>". $common_target ."</b>. <br><br>We made 50 thousand installs in US for 5 days. Then game reached  top-7 puzzle games in US and got 90 thousands live installs during the next week.<br>";
			$appanie = "https://www.appannie.com/apps/google-play/app/air.com.escapefearhouse/rank-history/#vtype=day&countries=US&start=2015-09-02&end=2015-11-16&view=rank&lm=7&start_date=2015-09-03&end_date=2015-09-27";
		?>
		<title><?php echo $title; ?></title>
		<meta name="description" content="<?php echo $description; ?>">


		<link rel="shortcut icon" href="/favicon.ico">


	</head>
	<body>

		<!-- Wrapper -->
		<div class="wrapper">

			<!-- Header -->
			<div class="header">
				<div class="container">

					<!-- Nav -->
					<?php include_once("../navigator-top.php"); ?>
					<!-- /Nav -->

				</div>
			</div>
			<!-- /Header -->

			<!-- Content -->
			<div class="content" style="padding: 0px 0px 275px;">

				<!-- Page Header -->
				<div class="page-header">
					<div class="container">
						<div class="in">
							<h1 class="case-header_title casetitle"><?php echo $h1; ?></h1>

							<div class="divider"></div>
							<div class="case-header_title casebrand"><img src="<?php echo $brand_src; ?>" alt="Иконка <?php echo $name; ?>">
								<br>
							<span style="font-size: 34%;">Target:</span></div>


						</div>
					</div>
				</div>
				<!-- /Page Header -->

				<!-- Offers -->
				<div style="margin-top: 152px;" class="one_case">
					<div class="container">

						<?php
							for ($i = 0; $i < count($tasks); ++$i)
								echo '<div class="caseheader"><h2 class="offers_h2 '. $flags[$i]. '">'. $tasks[$i]. '</h2></div>';
						?>
						<br>
						<div class="offers_list">
							<div style="min-height: 300px; margin: -15px 0px;" class="case-infogra">
								<div style="width: 50%; float: left; min-height: 349px; background: transparent url(<?php echo $shot; ?>) no-repeat scroll 0% 0px; margin-top: 5px;" class="case-info-image"></div>
								<div style="text-align: left; padding-left: 52%; min-height: 400px;" class="case-info">
									<h2 class="case-info-h2" style="font-family: &quot;Roboto Slab&quot;,sans-serif; font-size: 142%; color: rgb(0, 108, 156); font-weight: 600; text-align: left; padding-bottom: 11px;">Project description</h2>
									<span style="line-height: 156%;"><?php echo $notes; ?></span>
									<div style="margin-top: 11%;" class="case-date">
										<h2 style="font-family: &quot;Roboto Slab&quot;,sans-serif; font-size: 142%; color: rgb(0, 108, 156); font-weight: 600; text-align: left; padding-bottom: 11px;">Time frame</h2>
										<span style="background: transparent url('//img.rocket4app.com/cases/title/case-time-header.png') no-repeat scroll 0% 0%; font-weight: 600; padding: 9px 13px; line-height: 215%; margin-left: -7px;"><?php echo $time; ?></span>
									</div>
								</div>
							</div>



						</div>

					</div>


					<div class="container case-dashed" style="border-top: 2px dashed #C0C0C0;padding: 23px 0px;">

						<h2 class="case-info-h2">Installs graph
						</h2>

						<div class="how-it-works_scheme">
							<center><img src="<?php echo $diagram; ?>" alt="Как это работает"></center></a>
						</div>

						<div class="how-it-works_text">
							<p class="case-stars-h2"><?php echo $common_target; ?> -  Successfully!
							</p>
						</div>

					</div>
				</div>
				<!-- /Cooperation -->

				<!-- Apply -->
				<div class="apply inner_shadow">
					<div class="container">

						<?php include_once('../sendform-yellow.php'); ?>

					</div>
				</div>
				<!-- /Apply -->

				<!-- Reviews -->
				<div class="reviews inner_shadow">
					<div class="container">

						<?php include_once('../reviews.php'); ?>

					</div>
				</div>
				<!-- /Reviews -->

				<!-- Advantages -->
				<div class="advantages inner_shadow">
					<div class="container">

						<?php include('../advantages.php'); ?>

					</div>
				</div>
				<!-- Advantages -->

			</div>
			<!-- /Content -->

		</div>
		<!-- /Wrapper -->

		<!-- Footer -->
		<div class="footer-wrapper">
			<div class="footer">
				<div class="container">

					<!-- Nav -->
					<?php include_once("../navigator-bottom.php"); ?>
					<!-- /Nav -->

				</div>
			</div>
		</div>
		<!-- /Footer -->

		<!-- Callback Popup -->
		<?php include_once("../callbackwnd.php"); ?>
		<!-- /Callback Popup -->

		<script type="text/javascript" src="/js/jquery-1.9.0.min.js"></script>
		<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
		<script type="text/javascript" src="/js/owl.carousel.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="/js/main.js"></script>


	</body></html>
