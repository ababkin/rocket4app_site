$(function () {

    // Placeholder
    if ($("input, textarea").length)
        $('input[placeholder], textarea[placeholder]').placeholder();

    // Exchange Table
    $(document).on("focus", ".exchange_table .form-control", function(e) {

        $(this).closest(".exchange_table").find("tr").removeClass("active");
        $(this).closest("tr").addClass("active");
    });

    // Reviews Slider
    $('.reviews_carousel').owlCarousel({
        pagination: false,
        navigation: true,
        items : 3
    });

    // Callback Click
    $(".js_callback").on("click", function(e) {
        e.preventDefault();
        var href = $(this).attr("href");
        var $this = $(href);
        $.fancybox({
            'autoSize': false,
            'autoHeight' : true,
            'width': 360,
            'scrolling': 'no',
            'fitToView': false,
            'padding': [0, 0, 0, 0],
            'href': href,
            helpers: {
                overlay: {
                    locked: false
                }
            },
            beforeShow: function() {
            }
        });
    });
    $(".popup_close").on("click", function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

    // Set Equals Subtitles in Cases list
    $(".cases_list .cases_row").each(function(index, elem) {
        setEqualHeight($(elem).find(".cases_cell .item_subtitle span"));
    });

});

// ---------------------------------------------------------------------------------------

$(window).load(function () {


});

// ---------------------------------------------------------------------------------------

$(window).resize(function () {


});

// Set equal heights for divs
function setEqualHeight(columns) {

    var tallestcolumn = 0;
    columns.css('height', 'auto');
    columns.each(function () {
        currentHeight = $(this).height();
        if (currentHeight > tallestcolumn) {
            tallestcolumn = currentHeight;
        }
    });
    columns.height(tallestcolumn);
}


