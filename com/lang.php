<?php

$domain = $_SERVER ['HTTP_HOST'];
if (strpos($domain, "rocket4app.ru") !== false)
{
	$altdomain = str_replace("rocket4app.ru", "rocket4app.com", $domain);
	$lang = "ru"; $alt_lang = "en";
}
else
{
	$altdomain = str_replace("rocket4app.com", "rocket4app.ru", $domain);
	$lang = "en"; $alt_lang = "ru";
}
$script = str_replace("index.php", "", $domain . $_SERVER['PHP_SELF']);
$altscript = str_replace("index.php", "", $altdomain . $_SERVER['PHP_SELF']);

?>
<div class="header_languages">
	<ul class="header_languages_ul">
		<li><a href="//<?php echo $script; ?>" class="active"><?php echo $lang; ?></a></li>
		<li><a href="//<?php echo $altscript; ?>"><?php echo $alt_lang; ?></a></li>
	</ul>
</div>
