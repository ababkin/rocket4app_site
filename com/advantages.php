<h2 class="advantages_title section_title" style="font-size:120%">
	Our company provides you with the advantage you need<br> <small>to achieve success!</small>
</h2>

<ul class="advantages_list">
	<li class="__1">Price for getting your app into the top rankings <br>The lowest price on the market 

	</li>
	<li class="__2">100% guarantee of excellent results 
	</li>
	<li class="__3">Extensive experience promoting apps 
	</li>
	<li class="__4">Ability to secure the number of downloads required to get your app into the top 10 

	</li>
</ul>