<a href="/" class="header_logo"><img
	src="//img.rocket4app.com/images/logo.png"
	alt="Rocket4App"></a>

<!-- Langs -->
<?php include("lang.php"); ?>
<!-- /Langs -->

<!-- Contacts -->
<div class="header_right">

	<div class="header_contacts">
		<!-- <div class="header_phone">Оператор в сети</div> -->
		<div class="header_callback">
			<a href="#popup-callback" class="btn_callback js_callback">Call me</a>
		</div>
	</div>

</div>

<div class="header_right lk">

	<div class="header_contacts">
		<!-- <div class="header_phone">Оператор в сети</div> -->
		<div class="header_callback">
			<a href="//my.rocket4app.com" class="btn_callback" style="background-color: rgba(0,0,0,0);">My profile</a>
		</div>
	</div>

</div><!-- /Contacts -->

<?php
	$whyus = "/#about-us";
	if (strpos($page, "promotion.php")) $whyus = "/about-us";
?>

<!-- Nav -->
<div class="header_nav" id="header_nav">
	<ul class="header_nav_ul">
		<li><a href="/googleplay/<?php //#about-top ?>">About Top Charts</a></li>
		<li><a href="<?php echo $whyus; //"/#about-us" ?>">Why us</a></li>
		<li><a href="/cases.php">Cases</a></li>
		<li><a href="/partners.php">Cooperation</a></li>
		<li><a href="/prices.php">Prices</a></li>
	</ul>
</div>
<!-- /Nav -->
