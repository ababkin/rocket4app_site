<div class="footer_left">

	<div class="footer_nav" id="footer_nav">
		<span>
			<ul class="footer_nav_ul">
				<li><a href="/installs/">Paid game and app installs</a></li>
				<li><a href="/keywords/">Search phrase optimizations (ASO)</a></li>
				<li><a href="/promotion.php">On app promotion</a></li>
				<li><a href="/promotion/">Promotion of mobile games</a></li>
				<li><a href="/partners.php">Partnership</a></li>
				<li><a href="/#about-top">On getting your app into the top rankings</a></li>
				<li><br></li>
				<li><a href="/prices.php">Price</a></li>
				<li><a href="/cases.php">Cases</a></li>
				<li><a href="/#about-us">Why us?</a></li>
			</ul>
			<ul class="footer_nav_ul">
			</ul>
		</span>
	</div>

	<!--div class="footer_social">
		<ul class="footer_social_ul">
		<li><a href="#" class="__fb" title="Facebook"></a></li>
		<li><a href="#" class="__in" title="LinkedIn"></a></li>
		<li><a href="#" class="__tw" title="Twitter"></a></li>
		<li><a href="#" class="__yt" title="Youtube"></a></li>
		</ul>
	</div-->

</div>

<div class="footer_right">

	<div itemscope itemtype="http://schema.org/Organization">
		<span itemprop="name" style="display: none;">Rocket4App</span> <a
			href="/" class="header_logo footer_logo"> <img
			src="//img.rocket4app.com/images/logo.png" alt="Rocket4App"
			itemprop="logo"></a> <a itemprop="url" href="/"></a>

		<div class="header_contacts footer_contacts">
			<div class="">
				<b>Moscow, Russia:</b> <span itemprop="telephone">+7 (495) 204-17-85</span>
			</div>
			<div class="">
				<b>Kiev, Ukraine:</b> <span itemprop="telephone">+38 (063) 183-43-62</span>
			</div>
			<div class="header_email">
				<b>e-mail:</b> <a itemprop="email"
					href="mailto:support@rocket4app.com">support@rocket4app.com</a>
			</div>
			<div class="header_skype">
				<b>skype:</b> <a href="skype:rocket4app">rocket4app</a>
			</div>
			<br>
			<div class="header_phone">Online</div>
			<div class="header_callback footer_callback">
				<a href="#popup-callback" class="btn_callback js_callback">Call me</a>
			</div>
		</div>
	</div>
</div>

<div class=""
	style="text-align: center; float: left; width: 70%; margin-left: 15%; /*! position: relative; */ margin-top: -43px;">
	<span style="">Our partner in SEO: <a
		href="http://www.seoadministrator.com"
		style="text-decoration: underline;">Semonitor</a></span>
</div>

<script type="application/ld+json">
	{
  "@context" : "http://schema.org",
  "@type" : "Organization",
  "url" : "https://rocket4app.com",
  "logo": "https://rocket4app.com/images/logo_black.png",
  "contactPoint" : [{
    "@type" : "ContactPoint",
    "telephone" : "+74994033917",
    "contactType" : "customer support",
	"contactOption": "TollFree"
  },
  {
    "@type" : "ContactPoint",
    "telephone" : "+380631834362",
    "contactType" : "customer support",
	"contactOption": "TollFree"
  }]
}
</script>

<!-- /Google Analytics -->
<!-- script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-63938217-2', 'auto');
	ga('send', 'pageview');
	
</script -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91013877-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- /Google Analytics -->

<!-- Код тега ремаркетинга Google -->
<!--
	С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
-->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 945538711;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
<script type="text/javascript"
	src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
	<div style="display: inline;">
		<img height="1" width="1" style="border-style: none;" alt=""
			src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/945538711/?value=0&amp;guid=ON&amp;script=0" />
	</div>
</noscript>

<!-- Yandex.Metrika informer -->
<!-- a href="https://metrika.yandex.ru/stat/?id=31274678&amp;from=informer"
	target="_blank" rel="nofollow"><img
	src="//mc.yandex.ru/informer/31274678/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
	style="width: 88px; height: 31px; border: 0;" alt="Яндекс.Метрика"
	title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
onclick="try{Ya.Metrika.informer({i:this,id:31274678,lang:'ru'});return false}catch(e){}" /></a -->
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter31274678 = new Ya.Metrika({id:31274678,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
				accurateTrackBounce:true});
			} catch(e) { }
		});
		
		var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
		
		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<!-- noscript>
	<div>
	<img src="//mc.yandex.ru/watch/31274678"
	style="position: absolute; left: -9999px;" alt="" />
	</div>
</noscript -->
<!-- /Yandex.Metrika counter -->